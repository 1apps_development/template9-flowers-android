package com.workdo.plant.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.CellBestSellersBinding
import com.workdo.plant.model.FeaturedProductsSub
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.bumptech.glide.Glide

class BestsellerAdapter(val context:Context, private val bestSellerList: ArrayList<FeaturedProductsSub>,
                        private val itemClick: (Int, String) -> Unit) :RecyclerView.Adapter<BestsellerAdapter.BestSellerViewHolder>() {


    inner class BestSellerViewHolder(val itemBinding: CellBestSellersBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

        fun bindItems(data:FeaturedProductsSub,itemClick: (Int, String) -> Unit,position:Int)= with(itemBinding)
        {
            var currency = SharePreference.getStringPref(context, SharePreference.currency_name).toString()

            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath)).into(itemBinding.ivBestSeller)
            itemBinding.tvProductName.text = data.name.toString()
            itemBinding.tvProductPrice.text =
                Utils.getPrice(data.finalPrice.toString())
            itemBinding.tvCurrency.text = currency
            itemBinding.tvVariant.text=data.variantName.toString()

            if (data.inWhishlist== true) {
                itemBinding.ivWishList.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_wishlist, null)
            } else {
                itemBinding.ivWishList.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_diswishlist, null)
            }

            if(Utils.isLogin(context))
            {
                itemBinding.ivWishList.show()
            }else
            {
                itemBinding.ivWishList.hide()

            }

            itemBinding.btnAddToCart.setSafeOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            itemBinding.ivWishList.setSafeOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            itemView.setSafeOnClickListener {
                itemClick(position, Constants.ItemClick)
            }

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BestSellerViewHolder {
        val view=CellBestSellersBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return BestSellerViewHolder(view)

    }

    override fun onBindViewHolder(holder: BestSellerViewHolder, position: Int) {

       holder.bindItems(bestSellerList[position],itemClick,position)
    }

    override fun getItemCount(): Int {
        return bestSellerList.size
    }
}