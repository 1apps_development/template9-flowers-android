package com.workdo.plant.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.databinding.CellCartItemBinding

class CartItemAdapter :RecyclerView.Adapter<CartItemAdapter.CartItemViewHolder>(){

    inner class CartItemViewHolder(val itemBinding:CellCartItemBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartItemViewHolder {
        val view=CellCartItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return CartItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: CartItemViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 3
    }
}