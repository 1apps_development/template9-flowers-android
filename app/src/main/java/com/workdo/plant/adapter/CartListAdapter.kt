package com.workdo.plant.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.CellShoppingcartBinding
import com.workdo.plant.model.ProductListItem
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.bumptech.glide.Glide


class CartListAdapter(
    private val context: Activity,
    private val providerList: ArrayList<ProductListItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<CartListAdapter.AllCateViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency).toString()

    inner class AllCateViewHolder(private val binding: CellShoppingcartBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: ProductListItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {

            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.image))
                .into(binding.ivCart)
            binding.tvCardProductName.text = data.name.toString()
            binding.tvCardProductsize.text = data.variantName.toString()
            binding.tvorderitem.text=data.qty.toString()
            if(!Utils.isLogin(context))
            {
                val finalPrice= data.finalPrice?.toDouble()!! * data.qty?.toInt()!!
                binding.tvShoopingCartPrice.text=currency.plus(" ").plus(Utils.getPrice(finalPrice.toString()).plus(" "))
            }else
            {
                binding.tvShoopingCartPrice.text=currency.plus(" ").plus(Utils.getPrice(data.finalPrice.toString()).plus(" "))

            }

            if(data.variantName.isNullOrEmpty()||data.variantName=="null")
            {
                binding.tvCardProductsize.hide()
            }
            binding.tvDelete.setSafeOnClickListener {
                itemClick(position, Constants.ItemDelete)
            }
            binding.ivminus.setSafeOnClickListener {
                itemClick(position, Constants.IvMinus)
            }
            binding.ivplus.setSafeOnClickListener {
                itemClick(position, Constants.IvPlus)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllCateViewHolder {
        val view =
            CellShoppingcartBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AllCateViewHolder(view)
    }

    override fun onBindViewHolder(holder: AllCateViewHolder, position: Int) {
        holder.bind(providerList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return providerList.size
    }
}