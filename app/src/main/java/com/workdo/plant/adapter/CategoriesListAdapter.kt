package com.workdo.plant.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.CellProductlistingBinding
import com.workdo.plant.model.HomeCategoriesItem
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.bumptech.glide.Glide


class CategoriesListAdapter(
    private val context: Activity,
    private val categoryList: ArrayList<HomeCategoriesItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<CategoriesListAdapter.AddressViewHolder>() {
    var firsttime = 0
    inner class AddressViewHolder(private val binding: CellProductlistingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("NotifyDataSetChanged")
        fun bind(
            data: HomeCategoriesItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            if (firsttime == 0) {
                for (i in 0 until categoryList.size) {
                    categoryList[0].isSelect = true
                }
            }
            if (data.isSelect) {
                binding.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bg_green_11, null)
                binding.tvProductName.setTextColor(ContextCompat.getColor(context, R.color.white))
                binding.ivCart.imageTintList= ColorStateList.valueOf(ResourcesCompat.getColor(itemView.context.resources,R.color.white,null))
            } else {
                binding.card.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bg_white_11, null)
                binding.tvProductName.setTextColor(ContextCompat.getColor(context, R.color.appcolor))
                binding.ivCart.imageTintList= ColorStateList.valueOf(ResourcesCompat.getColor(itemView.context.resources,R.color.appcolor,null))

            }
            binding.tvProductName.text = data.name
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.iconPath)).into(binding.ivCart)

            itemView.setSafeOnClickListener {
                firsttime=1
                categoryList[0].isSelect = false
                for (element in categoryList) {
                    element.isSelect = false
                }
                data.isSelect = true
                notifyDataSetChanged()

                itemClick(position, Constants.ItemClick)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val view = CellProductlistingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AddressViewHolder(view)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.bind(categoryList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return categoryList.size
    }
}