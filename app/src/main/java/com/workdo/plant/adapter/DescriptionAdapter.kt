package com.workdo.plant.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.R
import com.workdo.plant.databinding.CellDescriptionBinding
import com.workdo.plant.model.OtherDescriptionArrayItem
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show


class DescriptionAdapter (val otherDescriptionArray:ArrayList<OtherDescriptionArrayItem>) :RecyclerView.Adapter<DescriptionAdapter.DescViewHolder>(){


    inner class DescViewHolder(val itemBinding: CellDescriptionBinding):RecyclerView.ViewHolder(itemBinding.root)
    {    var firsttime = 0

        fun bindItems(data:OtherDescriptionArrayItem,position: Int)= with(itemBinding)
        {
            itemBinding.tvDescription.text=data.description.toString()
            itemBinding.tvDescTitle.text=data.title.toString()
            val isVisible: Boolean = data.expand
            if (firsttime == 0) {
                for (i in 0 until otherDescriptionArray.size) {
                    otherDescriptionArray[0].expand = true
                }
            } else {

            }
            if (data.expand){
                itemBinding.tvDescription.show()
                itemBinding.ivPlus.setImageDrawable(ResourcesCompat.getDrawable(itemView.context.resources,
                    R.drawable.ic_minus,null))
            }else{
                itemBinding.tvDescription.hide()
                itemBinding.ivPlus.setImageDrawable(ResourcesCompat.getDrawable(itemView.context.resources,
                    R.drawable.ic_plus,null))
            }
           /* if(isVisible)
            {
                itemBinding.ivPlus.setImageDrawable(ResourcesCompat.getDrawable(itemView.context.resources,
                    R.drawable.ic_minus,null))
            }else
            {
                itemBinding.ivPlus.setImageDrawable(ResourcesCompat.getDrawable(itemView.context.resources,
                    R.drawable.ic_plus,null))
            }*/

           // itemBinding.tvDescription.visibility = if (isVisible) View.VISIBLE else View.GONE

            itemBinding.ivPlus.setSafeOnClickListener {
                firsttime=1
                if(!data.expand)
                {

                    for(i in 0 until otherDescriptionArray.size)
                    {
                        otherDescriptionArray[i].expand=false
                    }


                    data.expand=true
                }else
                {
                    data.expand=false

                }
                notifyDataSetChanged()
            }

            itemBinding.clmain.setSafeOnClickListener {
                firsttime=1
                if(!data.expand)
                {

                    for(i in 0 until otherDescriptionArray.size)
                    {
                        otherDescriptionArray[i].expand=false
                    }



                    data.expand=true
                }else
                {
                    data.expand=false

                }
                notifyDataSetChanged()
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DescViewHolder {
        val view =CellDescriptionBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return DescViewHolder(view)
    }

    override fun onBindViewHolder(holder: DescViewHolder, position: Int) {
        holder.bindItems(otherDescriptionArray[position],position)
    }



    override fun getItemCount(): Int {
        return otherDescriptionArray.size
    }
}