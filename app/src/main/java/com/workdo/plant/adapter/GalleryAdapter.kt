package com.workdo.plant.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.CellGalleryBinding
import com.workdo.plant.model.ProductImageItem
import com.bumptech.glide.Glide



class GalleryAdapter(val imageList: ArrayList<ProductImageItem>) :
    RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder>(){

    inner class GalleryViewHolder(val itemBinding: CellGalleryBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bindItems(data: ProductImageItem) = with(itemBinding)
        {

            Glide.with(itemView.context).load(ApiClient.ImageURL.BASE_URL.plus(data.imagePath))
                .into(ivProduct)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryViewHolder {
        val view = CellGalleryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GalleryViewHolder(view)
    }

    override fun onBindViewHolder(holder: GalleryViewHolder, position: Int) {

        holder.bindItems(imageList[position])
    }

    override fun getItemCount(): Int {

        return imageList.size
    }
}