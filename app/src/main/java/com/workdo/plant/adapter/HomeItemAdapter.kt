package com.workdo.plant.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.ui.activity.ActProductDetail
import com.workdo.plant.databinding.CellProductBinding
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener

class HomeItemAdapter(val context:Context) :RecyclerView.Adapter<HomeItemAdapter.HomeItemViewHolder>(){
    inner class HomeItemViewHolder(binding:CellProductBinding):RecyclerView.ViewHolder(binding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeItemViewHolder {
        val view=CellProductBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return HomeItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: HomeItemViewHolder, position: Int) {

        holder.itemView.setSafeOnClickListener {
            context.startActivity(Intent(context,ActProductDetail::class.java))
        }
    }

    override fun getItemCount(): Int {
        return 2
    }
}