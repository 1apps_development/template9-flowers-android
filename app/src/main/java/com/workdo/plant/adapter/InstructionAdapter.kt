package com.workdo.plant.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.CellInstructionBinding
import com.workdo.plant.model.ProductInstruction
import com.bumptech.glide.Glide

class InstructionAdapter(val instructionList: ArrayList<ProductInstruction>) :
    RecyclerView.Adapter<InstructionAdapter.InstructionViewHolder>() {
    inner class InstructionViewHolder(val binding: CellInstructionBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItems(data: ProductInstruction) = with(binding)
        {
            Glide.with(itemView.context).load(ApiClient.ImageURL.BASE_URL.plus(data.imagePath))
                .placeholder(R.drawable.intruction_icon)
                .into(ivInstruction)
            tvDescription.text = data.description

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InstructionViewHolder {
        val view =
            CellInstructionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return InstructionViewHolder(view)
    }

    override fun onBindViewHolder(holder: InstructionViewHolder, position: Int) {
        holder.bindItems(instructionList[position])

    }

    override fun getItemCount(): Int {
        return instructionList.size
    }
}