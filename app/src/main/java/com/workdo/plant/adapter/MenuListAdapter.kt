package com.workdo.plant.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.R
import com.workdo.plant.model.DataItems
import com.workdo.plant.ui.activity.ActCategoryProduct
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener

class MenuListAdapter(
    var context: Activity,
    private val mList: List<DataItems>,
    private val onFilterClick: (Int, String) -> Unit
) : RecyclerView.Adapter<MenuListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_menulist, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val categoriesModel = mList[position]
        holder.tvProductName.text = categoriesModel.name
        holder.itemView.setSafeOnClickListener {
            val intent = Intent(holder.itemView.context, ActCategoryProduct::class.java)
            intent.putExtra("main_id", categoriesModel.id.toString())
            intent.putExtra("maincategory_id", categoriesModel.id.toString())
            intent.putExtra("sub_id","0")
            intent.putExtra("name",categoriesModel.name.toString())
            intent.putExtra("menu", "menu")
            holder.itemView.context.startActivity(intent)
            context.finish()

        }


    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvProductName: TextView = itemView.findViewById(R.id.tvProductName)
    }

    private fun onFilterClick(id: Int, name: String) {
        onFilterClick.invoke(id, name)
    }
}