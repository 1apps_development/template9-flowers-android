package com.workdo.plant.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.model.SubCategoryItems
import com.bumptech.glide.Glide


class MenuSubListAdapter(
    var context: Activity,
    private val mList: List<SubCategoryItems>,
) : RecyclerView.Adapter<MenuSubListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_menusublist, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val categoriesModel = mList[position]
        holder.tvCategoriesname.text = categoriesModel.name
        Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(categoriesModel.iconImgPath)).into(holder.ivcategories)

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvCategoriesname: TextView = itemView.findViewById(R.id.tvCategoriesname)
        val ivcategories: ImageView = itemView.findViewById(R.id.ivcategories)
    }


}