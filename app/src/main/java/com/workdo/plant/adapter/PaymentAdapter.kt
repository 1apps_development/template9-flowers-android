package com.workdo.plant.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.CellPaymentBinding
import com.workdo.plant.model.PaymentData
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.SharePreference
import com.bumptech.glide.Glide


class PaymentAdapter(
    private val context: Activity,
    private val paymentList: ArrayList<PaymentData>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<PaymentAdapter.AddressViewHolder>() {
    var firsttime = 0

    inner class AddressViewHolder(private val binding: CellPaymentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: PaymentData,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            if (firsttime == 0) {
                for (i in 0 until paymentList.size) {
                    paymentList[0].isSelect = true
                }
            } else {

            }
            if (data.status == "off") {

            }
            if (data.isSelect == true) {
                binding.ivChecked.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        itemView.context.resources,
                        R.drawable.ic_round_checked, null
                    )
                )
                binding.clMain.background =
                    ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.border_lightpink_10,
                        null
                    )
            } else {
                binding.ivChecked.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        itemView.context.resources,
                        R.drawable.ic_round_unchecked, null
                    )

                )
                binding.clMain.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.bg_gray_10, null)
            }
            Glide.with(context).load(ApiClient.ImageURL.paymentUrl.plus(data.image))
                .into(binding.ivPaymentType)
            binding.tvPaymentDesc.text = paymentList[position].detail
            binding.tvPaymentName.text = paymentList[position].nameString
            itemView.setSafeOnClickListener {
                firsttime = 1
                paymentList[0].isSelect = false
                for (element in paymentList) {
                    element.isSelect = false

                }
                data.isSelect = true
                notifyDataSetChanged()
                itemClick(position, Constants.ItemClick)
            }
            if (data.isSelect == true) {
                if (data.isSelect == true) {
                    var paymentName = data.name.toString()
                    var stripeKey = data.stripePublishableKey.toString()
                    Log.e("PaymnetType", paymentName)
                    SharePreference.setStringPref(context, SharePreference.Payment_Type, paymentName)
                    SharePreference.setStringPref(context, SharePreference.stripeKey, stripeKey)
                    SharePreference.setStringPref(context, SharePreference.PaymentImage, paymentList[position].image.toString())
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressViewHolder {
        val view =
            CellPaymentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AddressViewHolder(view)
    }

    override fun onBindViewHolder(holder: AddressViewHolder, position: Int) {
        holder.bind(paymentList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return paymentList.size
    }
}