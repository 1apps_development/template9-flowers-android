package com.workdo.plant.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.databinding.CellRattingBinding
import com.workdo.plant.model.ProductReview

class RattingAdapter(
    var context: Activity,
    private val rattingList: ArrayList<ProductReview>,
    private val onItemClick: (String, String) -> Unit
) : RecyclerView.Adapter<RattingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = CellRattingBinding.inflate(LayoutInflater.from(parent.context),parent,false)

        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindItems(rattingList[position])

    }

    override fun getItemCount(): Int {
        return rattingList.size
    }

    class ViewHolder(val binding: CellRattingBinding) : RecyclerView.ViewHolder(binding.root) {


        fun bindItems(data :ProductReview)= with(binding)
        {
//            binding.tvTitle.text=data.title.toString()
            binding.tvReviewMessage.text=data.review.toString()
            binding.tvRattingUserName.text=data.userName.toString()

            binding.ivRatting.rating=data.rating?.toFloat()?:0.0f

        }

    }

    private fun onFilterClick(id: String, name: String) {
        onItemClick.invoke(id, name)
    }
}