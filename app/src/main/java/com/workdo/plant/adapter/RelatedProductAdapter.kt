package com.workdo.plant.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.CellHomeProductBinding
import com.workdo.plant.model.FeaturedProductsSub
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.invisible
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.bumptech.glide.Glide

class RelatedProductAdapter (private val context: Activity, private val itemList: ArrayList<FeaturedProductsSub>,
                             private val itemClick: (Int, String) -> Unit) : RecyclerView.Adapter<RelatedProductAdapter.RelatedProductViewHolder>(){


    inner class RelatedProductViewHolder(private val binding: CellHomeProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            val currency = SharePreference.getStringPref(context, SharePreference.currency_name).toString()

            Log.e("Cuurent", currency)
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath))
                .into(binding.ivProduct)
            binding.tvProductName.text = data.name.toString()
            binding.tvProductPrice.text =
                Utils.getPrice(data.finalPrice.toString())
            binding.tvCurrency.text = currency
            binding.tvTagName.text = data.variantName.toString()

            if (data.inWhishlist== true) {
                binding.ivWishlist.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_wishlist, null)
            } else {
                binding.ivWishlist.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_diswishlist, null)
            }
            if(data.variantName.isNullOrEmpty())
            {
                tvTagName.invisible()
            }
            if(Utils.isLogin(context))
            {
                binding.ivWishlist.show()
            }else
            {
                binding.ivWishlist.hide()

            }
            binding.ivWishlist.setSafeOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            binding.btnAddToCart.setSafeOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            itemView.setSafeOnClickListener {
                itemClick(position, Constants.ItemClick)
            }




        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RelatedProductViewHolder {

        val view=CellHomeProductBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return RelatedProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: RelatedProductViewHolder, position: Int) {

        holder.bind(itemList[position],context,position,itemClick)
    }

    override fun getItemCount(): Int {
        return itemList.size

    }
}