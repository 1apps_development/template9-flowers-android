package com.workdo.plant.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.CellProductBinding
import com.workdo.plant.model.FeaturedProductsSub
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.bumptech.glide.Glide

class SearchListAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<SearchListAdapter.ViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency).toString()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = CellProductBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(providerList[position], context, position, itemClick)

    }

    override fun getItemCount(): Int {
        return providerList.size
    }

    inner class ViewHolder(private val binding: CellProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {
            Log.e("Cuurent", currency.toString())
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath))
                .into(binding.ivPlant)
            binding.tvFeaturedProductName.text = data.name.toString()
            binding.tvFeaturedProductPrice.text =Utils.getPrice(data.finalPrice.toString())
            binding.tvCurrency.text = currency

           // binding.tvTag.text = data.tagApi
            if (data.inWhishlist == true) {
                binding.ivWishList.background =
                    androidx.core.content.res.ResourcesCompat.getDrawable(
                        context.resources,R.drawable.ic_wishlist,
                        null
                    )
            } else {
                binding.ivWishList.background =
                    androidx.core.content.res.ResourcesCompat.getDrawable(
                        context.resources, R.drawable.ic_diswishlist,
                        null
                    )
            }


            if (Utils.isLogin(context)) {
                binding.ivWishList.show()
            } else {
                binding.ivWishList.hide()

            }

            binding.ivWishList.setSafeOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            itemView.setSafeOnClickListener {
                itemClick(position, Constants.ItemClick)
            }

        }
    }

}