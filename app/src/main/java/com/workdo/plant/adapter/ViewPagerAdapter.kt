package com.workdo.plant.adapter


import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.PagerAdapter
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.model.ProductImageItem
import com.workdo.plant.ui.activity.ActImageSlider
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.bumptech.glide.Glide



class ViewPagerAdapter(var pagesList: ArrayList<ProductImageItem>, var context: Context) : PagerAdapter() {
    override fun getCount(): Int {
        return pagesList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as LinearLayout
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): View {
        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = layoutInflater.inflate(R.layout.cell_viewpager, container, false)
        val imageView: ImageView = view.findViewById(R.id.ivBanner)
        Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(pagesList[position].imagePath)).into(imageView)
        imageView.setSafeOnClickListener {
            val intent = Intent(context, ActImageSlider::class.java)
            intent.putParcelableArrayListExtra("imageList", pagesList)
            context.startActivity(intent)
        }
        container.addView(view)
        return view
    }
}