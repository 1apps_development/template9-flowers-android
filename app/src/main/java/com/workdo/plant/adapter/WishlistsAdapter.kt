package com.workdo.plant.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.CellWishlistBinding
import com.workdo.plant.model.WishListDataItem
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.bumptech.glide.Glide


class WishlistsAdapter(
    private val context: Activity,
    private val wishlist: ArrayList<WishListDataItem>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<WishlistsAdapter.WishlistViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency)

    inner class WishlistViewHolder(private val binding: CellWishlistBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: WishListDataItem,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)

        {

            binding.tvWishlist.text = data.productName
            binding.tvVariantName.text = data.variantName
            binding.tvPrice.text = currency.plus(Utils.getPrice(data.finalPrice.toString()))
            Glide.with(context)
                .load(ApiClient.ImageURL.BASE_URL.plus(data.productImage)).into(binding.ivWishList)
            binding.tvDelete.setSafeOnClickListener {
                itemClick(position, Constants.ItemDelete)
            }
            binding.tvAddtocart.setSafeOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            binding.cl1.setSafeOnClickListener {
                itemClick(position, Constants.ItemClick)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WishlistViewHolder {
        val view =
            CellWishlistBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WishlistViewHolder(view)
    }

    override fun onBindViewHolder(holder: WishlistViewHolder, position: Int) {
        holder.bind(wishlist[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return wishlist.size
    }
}