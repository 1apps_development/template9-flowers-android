package com.workdo.plant.api

import android.content.Context
import com.workdo.plant.util.SharePreference
import com.google.gson.GsonBuilder
import com.workdo.plant.remote.NetworkResponseAdapterFactory
import com.workdo.plant.util.MyApplication
import com.workdo.plant.util.Utils
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {
    private const val BASE_URL = "https://apps.rajodiya.com/ecommercego-mobileapp/api/"
    private const val API_URL = BASE_URL + "garden/"

    private var TIMEOUT: Long = 60 * 1 * 1.toLong()

    fun getClient(context: Context): ApiInterface {
        val remoteUrl = SharePreference.getStringPref(context, SharePreference.BaseUrl).toString()

        val headerInterceptor = Interceptor { chain ->
            var request = chain.request()
            request = request.newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", "Bearer " + SharePreference.getStringPref(context, SharePreference.token))
                .build()
            val response = chain.proceed(request)
            if(response.code==401)
            {
                Utils.logOut(context)
            }
            response
        }

        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        val httpClient = OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
        httpClient.addInterceptor(headerInterceptor)
        httpClient.addInterceptor(logging)
        val gson = GsonBuilder().setLenient().create()
        val retrofit = Retrofit.Builder()
            .baseUrl(remoteUrl)
            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .build()
        return retrofit.create(ApiInterface::class.java)
    }
    object ImageURL {
        val imageUrl = SharePreference.getStringPref(MyApplication.getInstance(), SharePreference.ImageUrl).toString()
        val paymentUrl = SharePreference.getStringPref(MyApplication.getInstance(), SharePreference.PaymentUrl).toString()

         val BASE_URL = imageUrl
    }


    val defaultClient: ApiInterface by lazy {
        val headerInterceptor = Interceptor { chain ->
            var request = chain.request()
            request = request.newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .build()
            val response = chain.proceed(request)
            response
        }

        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        val httpClient = OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
        httpClient.addInterceptor(headerInterceptor)
        httpClient.addInterceptor(logging)
        val gson = GsonBuilder().setLenient().create()
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .build()
        retrofit.create(ApiInterface::class.java)

    }

    fun getClientPayment(context: Context): ApiInterface {

        val headerInterceptor = Interceptor { chain ->
            var request = chain.request()
            request = request.newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "application/json")
                .build()
            val response = chain.proceed(request)
            response
        }

        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        val httpClient = OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
        httpClient.addInterceptor(headerInterceptor)
        httpClient.addInterceptor(logging)
        val gson = GsonBuilder().setLenient().create()
        val retrofit = Retrofit.Builder()
            .baseUrl(API_URL)
            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(NetworkResponseAdapterFactory())
            .build()
        return retrofit.create(ApiInterface::class.java)
    }
}


