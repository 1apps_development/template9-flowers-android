package com.workdo.plant.model

import com.google.gson.annotations.SerializedName

data class LandingPageModel(

	@field:SerializedName("data")
	val data: HomePageData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class HomepageSlider(

	@field:SerializedName("homepage-slider-title-text")
	val homepageSliderTitleText: String? = null,

	@field:SerializedName("homepage-slider-heading")
	val homepageSliderHeading: String? = null,

	@field:SerializedName("homepage-slider-sub-text")
	val homepageSliderSubText: String? = null
)

data class HomepageBanner(

	@field:SerializedName("homepage-banner-title-text")
	val homepageBannerTitleText: String? = null,

	@field:SerializedName("homepage-banner-btn-text")
	val homepageBannerBtnText: String? = null,

	@field:SerializedName("homepage-banner-bg-img")
	val homepageBannerBgImg: ArrayList<String>? = null,

	@field:SerializedName("homepage-banner-heading")
	val homepageBannerHeading: String? = null,

	@field:SerializedName("homepage-banner-sub-text")
	val homepageBannerSubText: String? = null
)

data class HomePageData(

	@field:SerializedName("them_json")
	val themJson: ThemJson? = null
)

data class HomepagePlantInstruction(

	@field:SerializedName("homepage-plant-instruction-image")
	val homepagePlantInstructionImage: ArrayList<String>? = null,

	@field:SerializedName("homepage-plant-instruction-description")
	val homepagePlantInstructionDescription: String? = null
)

data class HomepageBestseller(

	@field:SerializedName("homepage-bestseller-heading")
	val homepageBestsellerHeading: String? = null
)

data class ThemJson(

	@field:SerializedName("homepage-header")
	val homepageHeader: HomepageHeader? = null,

	@field:SerializedName("homepage-newsletter")
	val homepageNewsletter: HomepageNewsletter? = null,

	@field:SerializedName("homepage-slider")
	val homepageSlider: HomepageSlider? = null,

	@field:SerializedName("homepage-banner")
	val homepageBanner: HomepageBanner? = null,

	@field:SerializedName("homepage-plant-instruction")
	val homepagePlantInstruction: HomepagePlantInstruction? = null,

	@field:SerializedName("homepage-bestseller")
	val homepageBestseller: HomepageBestseller? = null
)

data class HomepageHeader(

	@field:SerializedName("homepage-header-bg-img")
	val homepageHeaderBgImg: ArrayList<String>? = null,

	@field:SerializedName("homepage-header")
	val homepageHeader: String? = null,

	@field:SerializedName("homepage-header-heading")
	val homepageHeaderHeading: String? = null,

	@field:SerializedName("homepage-header-sub-text")
	val homepageHeaderSubText: String? = null,

	@field:SerializedName("homepage-header-btn-text")
	val homepageHeaderBtnText: String? = null
)

data class HomepageNewsletter(

	@field:SerializedName("homepage-newsletter-sub-text")
	val homepageNewsletterSubText: String? = null,

	@field:SerializedName("homepage-newsletter-description")
	val homepageNewsletterDescription: String? = null,

	@field:SerializedName("homepage-newsletter-title-text")
	val homepageNewsletterTitleText: String? = null
)
