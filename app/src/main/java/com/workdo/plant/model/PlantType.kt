package com.workdo.plant.model

data class PlantType(
    var id:Int,
    var image:Int,
    var name:String
)
