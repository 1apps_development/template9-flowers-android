package com.workdo.plant.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.plant.adapter.CartItemAdapter

import com.workdo.plant.databinding.ActCartBinding
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener

class ActCart : AppCompatActivity() {
private lateinit var binding: ActCartBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActCartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupAdapter()
        initClickListener()
    }


    private fun setupAdapter()
    {
        binding.rvCart.apply {
            layoutManager=LinearLayoutManager(this@ActCart)
            adapter=CartItemAdapter()
        }
    }

    private fun initClickListener()
    {
        binding.btnAllProducts.setSafeOnClickListener {
            startActivity(Intent(this@ActCart,ActShoppingCart::class.java))
        }
    }
}