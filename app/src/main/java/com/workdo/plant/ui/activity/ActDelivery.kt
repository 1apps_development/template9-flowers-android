package com.workdo.plant.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.plant.R
import com.workdo.plant.adapter.DeliveryAdapter
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.ActSelectDeliveryOptionBinding
import com.workdo.plant.model.DeliveryData
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import kotlinx.coroutines.launch


class ActDelivery : AppCompatActivity() {

    private var deliveryList = ArrayList<DeliveryData>()
    private lateinit var deliveryAdapter: DeliveryAdapter

    private var manager: GridLayoutManager? = null

    var comment = ""
    private lateinit var _binding: ActSelectDeliveryOptionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActSelectDeliveryOptionBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        init()
    }

    private fun init() {
        _binding.ivBack.setSafeOnClickListener { finish() }


        _binding.btnContinue.setSafeOnClickListener {
            comment = _binding.edNote.text.toString()
            SharePreference.setStringPref(
                this@ActDelivery,
                SharePreference.Delivery_Comment,
                comment.toString()
            )
            startActivity(Intent(this@ActDelivery,ActPayment::class.java))

        }



        manager = GridLayoutManager(this@ActDelivery, 1, GridLayoutManager.VERTICAL, false)

    }

    //TODO delivery list api
    private fun callDeliveryList() {
        Utils.showLoadingProgress(this@ActDelivery)
        val hashMap = HashMap<String, String>()
        hashMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActDelivery).deliveryList(hashMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val paymentListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.size ?: 0) > 0) {
                                _binding.rvDelivery.show()
                                _binding.vieww.hide()
                                paymentListResponse.data?.let {
                                    deliveryList.addAll(it)
                                }
                            } else {
                                _binding.rvDelivery.hide()
                                _binding.vieww.show()
                            }
                            deliveryAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(this@ActDelivery, paymentListResponse.message.toString())
                        }
                        9 -> {
                            Utils.errorAlert(this@ActDelivery, response.body.message.toString())
                            startActivity(Intent(this@ActDelivery,ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActDelivery)
                    } else {
                        Utils.errorAlert(this@ActDelivery, response.body.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActDelivery, resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActDelivery, "Something went wrong")
                }
            }
        }
    }

    //TODO delivery list adapter
    private fun deliveryListAdapter(deliveryList: ArrayList<DeliveryData>) {
        _binding.rvDelivery.layoutManager = manager
        deliveryAdapter = DeliveryAdapter(this@ActDelivery, deliveryList) { i: Int, s: String ->
            if (deliveryList[i].isSelect == true) {
                SharePreference.setStringPref(this@ActDelivery, SharePreference.Delivery_Id, deliveryList[i].id.toString())
                SharePreference.setStringPref(this@ActDelivery, SharePreference.DeliveryImage, deliveryList[i].imagePath.toString())
            }
        }
        _binding.rvDelivery.adapter = deliveryAdapter
    }

    override fun onResume() {
        super.onResume()
        deliveryList.clear()
        deliveryListAdapter(deliveryList)
        callDeliveryList()
    }



}