package com.workdo.plant.ui.activity

import android.util.Log
import android.view.View
import com.workdo.plant.api.ApiClient
import com.workdo.plant.base.BaseActivity
import com.workdo.plant.databinding.ActImageSliderBinding
import com.workdo.plant.model.ProductImageItem
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel


class ActImageSlider : BaseActivity() {
    private lateinit var imageSliderBinding: ActImageSliderBinding
    var imgList: ArrayList<ProductImageItem>? = null

    override fun setLayout(): View = imageSliderBinding.root

    override fun initView() {
        imageSliderBinding = ActImageSliderBinding.inflate(layoutInflater)
        imgList = intent.getParcelableArrayListExtra("imageList")
        Log.e("ImageList",imgList.toString())
        val imageList = ArrayList<SlideModel>()
        for (i in 0 until imgList?.size!!) {
            val slideModel = SlideModel(ApiClient.ImageURL.BASE_URL.plus(imgList!![i].imagePath))
            imageList.add(slideModel)
        }
        imageSliderBinding.imageSlider.setImageList(imageList,ScaleTypes.FIT)
        imageSliderBinding.ivCancle.setSafeOnClickListener {
            finish()
        }
    }
}