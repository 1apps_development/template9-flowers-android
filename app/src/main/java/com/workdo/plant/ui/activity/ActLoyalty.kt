package com.workdo.plant.ui.activity

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.plant.R
import com.workdo.plant.adapter.LoyaltyListAdapter
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.ActLoyaltyProgramBinding
import com.workdo.plant.model.OrderListData
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.PaginationScrollListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import kotlinx.coroutines.launch
import java.util.HashMap

class ActLoyalty : AppCompatActivity() {
    private lateinit var _binding: ActLoyaltyProgramBinding
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    private var orderList = ArrayList<OrderListData>()
    private lateinit var loyaltyListAdapter: LoyaltyListAdapter
    private var manager: LinearLayoutManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding= ActLoyaltyProgramBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        init()
    }



    private fun init() {
        manager = LinearLayoutManager(this@ActLoyalty)
        orderListAdapter(orderList)
        pagination()
        _binding.ivBack.setSafeOnClickListener { finish() }
        _binding.btnCopyURl.setSafeOnClickListener {
            dlgCopyOrderNumber(_binding.tvURL.text.toString())

        }

        loyalityProgram()
    }

    //TODO Copy order number dialog
    private fun dlgCopyOrderNumber(message: String?) {
        val builder = AlertDialog.Builder(this@ActLoyalty)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.url_copied)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("Url_copied", message)
            clipboard.setPrimaryClip(clip)
        }

        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callOrderList()
            }
        }
        _binding.rvLoyalty.addOnScrollListener(paginationListener)
    }

    //TODO delivery list api
    private fun loyalityProgram() {
        Utils.showLoadingProgress(this@ActLoyalty)
        val hashMap = HashMap<String, String>()
        hashMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActLoyalty)
                .loyalityProgramJson(hashMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val loyalityProgramResponse = response.body.data?.loyalityProgram
                    when (response.body.status) {
                        1 -> {
                            _binding.tvProgramTitle.text =
                                loyalityProgramResponse?.loyalityProgramTitle
                            _binding.tvProgramDesc.text =
                                loyalityProgramResponse?.loyalityProgramDescription

                            _binding.tvYourFriend.text =
                                loyalityProgramResponse?.loyalityProgramCopyThisLinkAndSendToYourFriends
                            loyalityReward(loyalityProgramResponse?.loyalityProgramYourCash)
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActLoyalty,
                                loyalityProgramResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActLoyalty,
                                response.body.message.toString()
                            )
                        startActivity(Intent(this@ActLoyalty,ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActLoyalty)
                    } else {
                        Utils.errorAlert(
                            this@ActLoyalty,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyalty,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyalty,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO Loyality reward api
    private fun loyalityReward(loyalityProgramYourCash: String?) {
        val loyalityReward = HashMap<String, String>()
        loyalityReward["user_id"] =
            SharePreference.getStringPref(this@ActLoyalty, SharePreference.userId)
                .toString()
        loyalityReward["theme_id"] =getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActLoyalty)
                .loyalityReward(loyalityReward)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val loyalityRewardResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            _binding.tvProgramYourCash.text =
                                loyalityProgramYourCash?.plus(": ").plus("+")
                                    .plus(loyalityRewardResponse?.point).plus(
                                        SharePreference.getStringPref(
                                            this@ActLoyalty,
                                            SharePreference.currency_name
                                        )
                                    )
                            _binding.tvURL.text = ApiClient.ImageURL.BASE_URL
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActLoyalty,
                                loyalityRewardResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActLoyalty,
                                response.body.message.toString()
                            )
                            startActivity(Intent(this@ActLoyalty,ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActLoyalty)
                    } else {
                        Utils.errorAlert(
                            this@ActLoyalty,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyalty,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyalty,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO order list api
    private fun callOrderList() {
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["user_id"] =
            SharePreference.getStringPref(this@ActLoyalty, SharePreference.userId).toString()
        categoriesProduct["theme_id"] =getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActLoyalty)
                .getOrderList(currentPage.toString(), categoriesProduct)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val orderListResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvLoyalty.show()
                                currentPage =
                                    orderListResponse?.currentPage!!.toInt()
                                total_pages =
                                    orderListResponse.lastPage!!.toInt()
                                orderListResponse.data?.let {
                                    orderList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvLoyalty.hide()
                            }
                            loyaltyListAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActLoyalty,
                                orderListResponse?.data?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActLoyalty,
                                response.body.message.toString()
                            )
                            startActivity(Intent(this@ActLoyalty,ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActLoyalty)
                    } else {
                        Utils.errorAlert(
                            this@ActLoyalty,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyalty,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActLoyalty,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        currentPage = 1
        isLastPage = false
        isLoading = false
        orderList.clear()
        callOrderList()
    }

    //TODO order list data set
    private fun orderListAdapter(orderHistorylist: ArrayList<OrderListData>) {
        _binding.rvLoyalty.layoutManager = manager
        loyaltyListAdapter = LoyaltyListAdapter(this@ActLoyalty, orderHistorylist) { i: Int, s: String -> }
        _binding.rvLoyalty.adapter = loyaltyListAdapter
    }


}