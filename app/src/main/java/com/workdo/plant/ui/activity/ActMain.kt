package com.workdo.plant.ui.activity

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.workdo.plant.R
import com.workdo.plant.databinding.ActMainBinding
import com.workdo.plant.ui.fragment.*
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.workdo.plant.util.Utils.openWelcomeScreen

class ActMain : AppCompatActivity() {
    private lateinit var _binding:ActMainBinding
    var pos = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding= ActMainBinding.inflate(layoutInflater)
        setContentView(_binding.root)

        val name = SharePreference.getStringPref(this@ActMain, SharePreference.userName)
        pos = intent.getStringExtra("pos").toString()

        if(SharePreference.getStringPref(this@ActMain,SharePreference.cartCount).isNullOrEmpty())
        {
            SharePreference.setStringPref(this@ActMain,SharePreference.cartCount,"0")
        }

        when (pos) {
            "1" -> {
                setCurrentFragment(FragHome())
                _binding.bottomNavigation.selectedItemId = R.id.ivHome
            }
            "2" -> {
                setCurrentFragment(FragProduct())
                _binding.bottomNavigation.selectedItemId = R.id.ivProduct
            }
            "3" -> {
                setCurrentFragment(FragAllCategories())
                _binding.bottomNavigation.selectedItemId = R.id.ivCategories
            }
            "4" -> {
                setCurrentFragment(FragWishList())
                _binding.bottomNavigation.selectedItemId = R.id.ivWishList
            }
            "5" -> {
                setCurrentFragment(FragSetting())
                _binding.bottomNavigation.selectedItemId = R.id.ivSettings
            }
            else -> {
                setCurrentFragment(FragHome())
            }
        }
        bottomSheetItemNavigation()
    }

    fun bottomNavigationSelection(pos:String)
    {
        when (pos) {
            "1" -> {
                _binding.bottomNavigation.selectedItemId = R.id.ivHome
            }
            "2" -> {
                _binding.bottomNavigation.selectedItemId = R.id.ivProduct
            }
            "3" -> {
                _binding.bottomNavigation.selectedItemId = R.id.ivCategories
            }
            "4" -> {
                _binding.bottomNavigation.selectedItemId = R.id.ivWishList
            }
            "5" -> {
                _binding.bottomNavigation.selectedItemId = R.id.ivSettings
            }
        }
    }




    private fun bottomSheetItemNavigation() {
        _binding.bottomNavigation.setOnItemSelectedListener { item ->
            val fragment = supportFragmentManager.findFragmentById(R.id.mainContainer)

            when (item.itemId) {
                R.id.ivHome -> {
                    if (fragment !is FragHome) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragHome())
                    }
                    true
                }
                R.id.ivProduct -> {
                    if (fragment !is FragProduct) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragProduct())
                    }
                    true
                }
                R.id.ivCategories -> {

                    if (fragment !is FragAllCategories) {
                        supportFragmentManager.fragments.clear()
                        setCurrentFragment(FragAllCategories())
                        SharePreference.setStringPref(this@ActMain,SharePreference.clickMenu,"click")
                    }else
                    {
                        fragment.showLayout()
                    }
                    true

                }
                R.id.ivWishList -> {
                    if (Utils.isLogin(this@ActMain)) {
                        if (fragment !is FragWishList) {
                            supportFragmentManager.fragments.clear()
                            setCurrentFragment(FragWishList())
                        }
                    } else {
                        openWelcomeScreen(this@ActMain)

                    }

                    true
                }
                R.id.ivSettings -> {
                    if (Utils.isLogin(this@ActMain)) {
                        if (fragment !is FragSetting) {
                            supportFragmentManager.fragments.clear()
                            setCurrentFragment(FragSetting())
                        }
                    } else {
                        openWelcomeScreen(this@ActMain)
                    }


                    true
                }


                else -> {
                    false
                }
            }
        }
    }





    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.mainContainer, fragment)
            commit()
        }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finishAffinity()
        } else {
            mExitDialog()
        }
    }

    private fun mExitDialog() {
        val builder = AlertDialog.Builder(this@ActMain)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.exit)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            builder.setCancelable(true)
            ActivityCompat.finishAfterTransition(this@ActMain)
            ActivityCompat.finishAffinity(this@ActMain);
            finish()
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
}