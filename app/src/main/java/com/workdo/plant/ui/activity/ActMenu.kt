package com.workdo.plant.ui.activity

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.plant.R

import com.workdo.plant.adapter.MenuListAdapter
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.ActMenuBinding
import com.workdo.plant.model.DataItems
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import kotlinx.coroutines.launch


class ActMenu : AppCompatActivity() {
    private lateinit var _binding: ActMenuBinding
    private var managerBestsellers: LinearLayoutManager? = null
    private var menuList = ArrayList<DataItems>()
    private lateinit var menuListAdapter: MenuListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActMenuBinding.inflate(layoutInflater)

        setContentView(_binding.root)
        init()
    }


    private fun init() {
        if (!Utils.isLogin(this@ActMenu)) {
            _binding.tvLogin.show()
        } else {
            _binding.tvLogin.hide()

        }


        _binding.ivClose.setSafeOnClickListener { finish() }

        _binding.tvLogin.setSafeOnClickListener {
            val intent = Intent(this@ActMenu, ActWelCome::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
            finish()
        }
        managerBestsellers = LinearLayoutManager(this@ActMenu)
        _binding.ivInstagram.setSafeOnClickListener {
            val contactUs =
                SharePreference.getStringPref(this@ActMenu, SharePreference.insta).toString()
            val uri: Uri =
                Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)

            startActivity(intent)
        }
        _binding.ivMessage.setSafeOnClickListener {
            val contactUs =
                SharePreference.getStringPref(this@ActMenu, SharePreference.messanger).toString()
            val uri: Uri = Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)
            startActivity(intent)
        }
        _binding.ivYoutube.setSafeOnClickListener {
            val contactUs =
                SharePreference.getStringPref(this@ActMenu, SharePreference.youtube).toString()
            val uri: Uri =
                Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)

            startActivity(intent)
        }
        _binding.ivTwitter.setSafeOnClickListener {
            val contactUs =
                SharePreference.getStringPref(this@ActMenu, SharePreference.twitter).toString()
            val uri: Uri = Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)
            startActivity(intent)
        }
    }

    //TODO Adapter set Best seller
    private fun menuAdapter(menuList: ArrayList<DataItems>) {
        _binding.rvMenulist.layoutManager = managerBestsellers
        menuListAdapter =
            MenuListAdapter(this@ActMenu, menuList) { i: Int, s: String ->
            }
        _binding.rvMenulist.adapter = menuListAdapter
    }

    //TODO navigation api
    private fun callNavigation() {
        Utils.showLoadingProgress(this@ActMenu)
        val hashMap = HashMap<String, String>()
        hashMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActMenu)
                .navigation(hashMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val stateListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            if ((stateListResponse.data?.size ?: 0) > 0) {
                                _binding.rvMenulist.show()
                                stateListResponse.data?.let { menuList.addAll(it) }
                            } else {
                                _binding.rvMenulist.hide()
                            }
                            menuListAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActMenu,
                                stateListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActMenu,
                                response.body.message.toString()
                            )
                            startActivity(Intent(this@ActMenu, ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActMenu)
                    } else {
                        Utils.errorAlert(
                            this@ActMenu,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenu,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenu,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        menuList.clear()
        menuAdapter(menuList)
        callNavigation()
    }


}