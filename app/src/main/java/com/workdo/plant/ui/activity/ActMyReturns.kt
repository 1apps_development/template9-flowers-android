package com.workdo.plant.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.plant.R
import com.workdo.plant.adapter.ReturnOrderlistAdapter
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.ActMyReturnsBinding
import com.workdo.plant.model.ReturnOrderItem
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import kotlinx.coroutines.launch


class ActMyReturns : AppCompatActivity() {
    private lateinit var _binding: ActMyReturnsBinding
    private var isLoading = false
    private var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    private var orderList = ArrayList<ReturnOrderItem>()
    private lateinit var returnOrderlistAdapter: ReturnOrderlistAdapter
    private var manager: LinearLayoutManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding= ActMyReturnsBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        init()

    }


    private fun init() {
        _binding.ivBack.setSafeOnClickListener { finish() }

        manager = LinearLayoutManager(this@ActMyReturns)
        nestedScrollViewPagination()

    }

    private fun nestedScrollViewPagination() {
        _binding.rvMyReturn.isNestedScrollingEnabled=false
        _binding.scrollView.viewTreeObserver.addOnScrollChangedListener {
            val view = _binding.scrollView.getChildAt(_binding.scrollView.childCount - 1)
            val diff =view.bottom - (_binding.scrollView.height + _binding.scrollView.scrollY)

            if (diff == 0 && !isLastPage && !isLoading){
                currentPage++
                callOrderList()
            }
        }
    }

    private fun callOrderList() {
        Utils.showLoadingProgress(this@ActMyReturns)
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["user_id"] = SharePreference.getStringPref(this@ActMyReturns, SharePreference.userId).toString()
        categoriesProduct["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when(val response = ApiClient.getClient(this@ActMyReturns).getReturnOrderList(currentPage.toString(),categoriesProduct)){
                is NetworkResponse.Success->{
                    Utils.dismissLoadingProgress()
                    val OrderListResponse = response.body.data
                    when(response.body.status){
                        1->{
                            if (response.body.data?.data?.size?:0 > 0) {
                                _binding.rvMyReturn.show()
                                _binding.tvNoDataFound.hide()
                                currentPage = OrderListResponse?.currentPage!!.toInt()
                                total_pages = OrderListResponse.lastPage!!.toInt()
                                OrderListResponse.data?.let {
                                    orderList.addAll(it)
                                }
                                if (currentPage >= total_pages){
                                    isLastPage=true
                                }
                                isLoading=false
                            }else
                            {
                                if(orderList.size==0)
                                {
                                    _binding.rvMyReturn.hide()
                                    _binding.tvNoDataFound.show()
                                }

                            }

                            returnOrderlistAdapter.notifyDataSetChanged()
                        }

                        0->{
                            Utils.errorAlert(this@ActMyReturns,OrderListResponse?.data?.get(0)?.message.toString())
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActMyReturns,
                                response.body.message.toString()
                            )
                            startActivity(Intent(this@ActMyReturns,ActWelCome::class.java))
                        }
                    }
                }
                is NetworkResponse.ApiError ->{
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9){
                        Utils.setInvalidToken(this@ActMyReturns)
                    }else{
                        Utils.errorAlert(this@ActMyReturns,response.body.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMyReturns,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMyReturns,
                        "Something went wrong"
                    )
                }

            }
        }
    }

    private fun returnOrderListAdapter(returnOrderList: ArrayList<ReturnOrderItem>) {
        _binding.rvMyReturn.layoutManager = manager
        returnOrderlistAdapter =
            ReturnOrderlistAdapter(this@ActMyReturns, returnOrderList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    startActivity(Intent(this@ActMyReturns,ActOrderDetails::class.java).putExtra("order_ID",returnOrderList[i].id.toString()))
                }
            }
        _binding.rvMyReturn.adapter = returnOrderlistAdapter
    }

    override fun onResume() {
        super.onResume()
        currentPage = 1
        isLastPage = false
        isLoading = false
        orderList.clear()
        returnOrderListAdapter(orderList)

        callOrderList()
    }







}