package com.workdo.plant.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.plant.R
import com.workdo.plant.adapter.OrderHistoryListAdapter
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.ActOrderHistoryBinding
import com.workdo.plant.model.OrderListData
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.PaginationScrollListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import kotlinx.coroutines.launch

class ActOrderHistory : AppCompatActivity() {
    private lateinit var _binding: ActOrderHistoryBinding
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    private var orderList = ArrayList<OrderListData>()
    private lateinit var orderlistAdapter: OrderHistoryListAdapter
    private var manager: LinearLayoutManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding= ActOrderHistoryBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        init()
    }


    private fun init() {
        manager = LinearLayoutManager(this@ActOrderHistory)

        pagination()
        _binding.ivBack.setSafeOnClickListener { finish() }
//        _binding.ivCart.setSafeOnClickListener { openActivity(ActCart::class.java) }
    }


    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callOrderList()
            }
        }
        _binding.rvOrderHistory.addOnScrollListener(paginationListener)
    }

    //TODO Order list api
    private fun callOrderList() {
        Utils.showLoadingProgress(this@ActOrderHistory)
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["user_id"] = SharePreference.getStringPref(this@ActOrderHistory, SharePreference.userId).toString()
        categoriesProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActOrderHistory)
                .getOrderList(currentPage.toString(), categoriesProduct)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val orderListResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvOrderHistory.show()
                                _binding.tvNoDataFound.hide()
                                currentPage =
                                    orderListResponse?.currentPage!!.toInt()
                                total_pages =
                                    orderListResponse.lastPage!!.toInt()
                                orderListResponse.data?.let {
                                    orderList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvOrderHistory.hide()
                                _binding.tvNoDataFound.show()
                            }
                            orderlistAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActOrderHistory,
                                orderListResponse?.data?.get(0)?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActOrderHistory,
                                response.body.message.toString()
                            )
                            startActivity(Intent(this@ActOrderHistory,ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActOrderHistory)
                    } else {
                        Utils.errorAlert(
                            this@ActOrderHistory,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActOrderHistory,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActOrderHistory,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        currentPage = 1
        isLastPage = false
        isLoading = false
        orderList.clear()
        orderListAdapter(orderList)
        callOrderList()
    }

    //TODO order list data set
    private fun orderListAdapter(orderHistorylist: ArrayList<OrderListData>) {
        _binding.rvOrderHistory.layoutManager = manager
        orderlistAdapter =
            OrderHistoryListAdapter(this@ActOrderHistory, orderHistorylist) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    startActivity(
                        Intent(
                            this@ActOrderHistory,
                            ActOrderDetails::class.java
                        ).putExtra("order_ID", orderHistorylist[i].id.toString())
                    )
                }
            }
        _binding.rvOrderHistory.adapter = orderlistAdapter
    }

}