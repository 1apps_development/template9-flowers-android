package com.workdo.plant.ui.activity

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.plant.R
import com.workdo.plant.adapter.PaymentAdapter
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.ActSelectPaymentOptionBinding
import com.workdo.plant.model.PaymentData
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import kotlinx.coroutines.launch


class ActPayment : AppCompatActivity() {
    private lateinit var _binding: ActSelectPaymentOptionBinding
    private var paymentList = ArrayList<PaymentData>()
    private lateinit var paymentAdapter: PaymentAdapter
    private var manager: GridLayoutManager? = null
    var comment = ""
    var paymentName = ""
    var stripeKey = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActSelectPaymentOptionBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        init()
    }


    private fun init() {
        Log.e("stripeKey", stripeKey)
        _binding.ivBack.setSafeOnClickListener { finish() }
        _binding.chbTermsCondition.isChecked = false
        _binding.chbTermsCondition.setSafeOnClickListener {
            _binding.btnContinue.isEnabled = _binding.chbTermsCondition.isChecked
        }

        _binding.btnContinue.setSafeOnClickListener {
            if (_binding.chbTermsCondition.isChecked) {
                comment = _binding.edNote.text.toString()
                SharePreference.setStringPref(this@ActPayment, SharePreference.Payment_Comment, comment)
                startActivity(Intent(this@ActPayment, ActConfirm::class.java))
            }
        }
        manager = GridLayoutManager(this@ActPayment, 1, GridLayoutManager.VERTICAL, false)
        _binding.tvTerms.setSafeOnClickListener {
            val terms = SharePreference.getStringPref(this@ActPayment, SharePreference.Terms).toString()
            Log.e("terms",terms.toString())
            val uri: Uri = Uri.parse(terms)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)
            startActivity(intent)
        }
    }

    //TODO payment list api
    private fun callPaymentList() {
        Utils.showLoadingProgress(this@ActPayment)
        val paymentListMap = HashMap<String, String>()
        paymentListMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActPayment)
                .paymentList(paymentListMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val paymentListResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.size ?: 0) > 0) {
                                _binding.rvPayment.show()
                                _binding.vieww.hide()

                                runOnUiThread {
                                    paymentListResponse.data?.let {
                                        paymentList.addAll(it)
                                    }
                                }
                                paymentList.removeAll {
                                    it.status == "off"
                                }
                                paymentListAdapter(paymentList)
                            } else {
                                _binding.rvPayment.hide()
                                _binding.vieww.show()
                            }
                            paymentAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActPayment,
                                paymentListResponse.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActPayment,
                                response.body.message.toString()
                            )
                            startActivity(Intent(this@ActPayment, ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActPayment)
                    } else {
                        Utils.errorAlert(
                            this@ActPayment,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActPayment,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActPayment,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO payment list data set
    private fun paymentListAdapter(paymentList: ArrayList<PaymentData>) {
        _binding.rvPayment.layoutManager = manager
        paymentAdapter =
            PaymentAdapter(this@ActPayment, paymentList) { i: Int, s: String ->
                if (paymentList[i].isSelect == true) {
                    paymentName = paymentList[i].nameString.toString()

                    SharePreference.setStringPref(
                        this@ActPayment,
                        SharePreference.Payment_Type,
                        paymentName
                    )

                    SharePreference.setStringPref(
                        this@ActPayment,
                        SharePreference.PaymentImage,
                        paymentList[i].image.toString()
                    )
                } else {

                }
            }
        _binding.rvPayment.adapter = paymentAdapter
    }

    override fun onResume() {
        super.onResume()
        paymentList.clear()
        callPaymentList()
    }
}