package com.workdo.plant.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.ActPersonalDetailBinding
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import kotlinx.coroutines.launch

class ActPersonalDetail : AppCompatActivity() {
    private lateinit var _binding: ActPersonalDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding= ActPersonalDetailBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        init()
    }

    private fun init() {
        _binding.ivBack.setSafeOnClickListener { finish() }
        _binding.edFirstName.setText(
            SharePreference.getStringPref(this@ActPersonalDetail, SharePreference.userFirstName)
                .toString()
        )
        _binding.edLastName.setText(
            SharePreference.getStringPref(
                this@ActPersonalDetail,
                SharePreference.userLastName
            ).toString()
        )
        _binding.edEmail.setText(
            SharePreference.getStringPref(
                this@ActPersonalDetail,
                SharePreference.userEmail
            ).toString()
        )
        _binding.edPhone.setText(
            SharePreference.getStringPref(
                this@ActPersonalDetail,
                SharePreference.userMobile
            ).toString()
        )
        _binding.btnEditProfile.setSafeOnClickListener {
            if (_binding.edFirstName.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_f_name))
            } else if (_binding.edLastName.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_l_name))
            } else if (_binding.edEmail.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_email))
            } else if (!Utils.isValidEmail(_binding.edEmail.text.toString())) {
                Utils.errorAlert(
                    this,
                    resources.getString(R.string.validation_valid_email)
                )
            } else if (_binding.edPhone.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_phone_number))
            } else {
                val editprofile = HashMap<String, String>()
                editprofile["user_id"] =
                    SharePreference.getStringPref(this@ActPersonalDetail, SharePreference.userId)
                        .toString()
                editprofile["first_name"] = _binding.edFirstName.text.toString()
                editprofile["last_name"] = _binding.edLastName.text.toString()
                editprofile["email"] = _binding.edEmail.text.toString()
                editprofile["telephone"] = _binding.edPhone.text.toString()
                editprofile["theme_id"] = getString(R.string.theme_id)
                callEditProfile(editprofile)
            }
        }
    }

    //TODO edit profile api
    private fun callEditProfile(editprofile: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActPersonalDetail)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActPersonalDetail).setProfileUpdate(editprofile)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val editProfileResponse = response.body.data?.data
                    when (response.body.status) {
                        1 -> {
                            Log.e("UserData", editProfileResponse.toString())
                            SharePreference.setStringPref(
                                this@ActPersonalDetail,
                                SharePreference.userName,
                                editProfileResponse?.name.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActPersonalDetail,
                                SharePreference.userFirstName,
                                editProfileResponse?.firstName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActPersonalDetail,
                                SharePreference.userLastName,
                                editProfileResponse?.lastName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActPersonalDetail,
                                SharePreference.userEmail,
                                editProfileResponse?.email.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActPersonalDetail,
                                SharePreference.userMobile,
                                editProfileResponse?.mobile.toString()
                            )
                            finish()
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActPersonalDetail,
                                response.body.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActPersonalDetail,
                                response.body.message.toString()
                            )
                            startActivity(Intent(this@ActPersonalDetail,ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActPersonalDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActPersonalDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActPersonalDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActPersonalDetail,
                        "Something went wrong"
                    )
                }
            }
        }
    }
}