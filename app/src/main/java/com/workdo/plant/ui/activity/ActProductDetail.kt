package com.workdo.plant.ui.activity

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.get
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.*
import androidx.viewpager.widget.ViewPager
import com.workdo.plant.R
import com.workdo.plant.adapter.*
import com.workdo.plant.adapter.variant.VariantAdapter
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.ActProductDetailBinding
import com.workdo.plant.databinding.DlgConfirmBinding
import com.workdo.plant.model.*
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.PaginationScrollListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class ActProductDetail : AppCompatActivity() {
    private lateinit var _binding: ActProductDetailBinding

    private lateinit var adapter: ViewPagerAdapter
    private lateinit var variantAdapter: VariantAdapter
    var rattingValue = "0"
    var product_id = ""
    var variant_id = ""
    var variantName = ""
    var finalPrice = ""
    var disCountPrice = ""
    var originalPrice = ""
    var productInfo = ProductInfo()
    var productStock = ""


    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    var count = 1
    private lateinit var relatedProductAdapter: RelatedProductAdapter
    private var manager: GridLayoutManager? = null
    private var relatedProductItemList = ArrayList<FeaturedProductsSub>()
    private var isWishList=false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActProductDetailBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        init()
        paginationRelatedItems()

    }
    private fun init() {
        if (Utils.isLogin(this@ActProductDetail)) {
            _binding.btnFeedback.show()
            _binding.ivWishlist.show()

        } else {
            _binding.btnFeedback.hide()
            _binding.ivWishlist.hide()

        }
        _binding.tvCount.text = SharePreference.getStringPref(this@ActProductDetail, SharePreference.cartCount).toString()
        manager = GridLayoutManager(this@ActProductDetail, 1, GridLayoutManager.HORIZONTAL, false)
        relatedProductAdapter(relatedProductItemList)
        initClickListeners()
        callProductDetailApi()
        callRelatedProductApi()
    }

    private fun setupDetailData(data: ProductDetailData) {
        _binding.tvProductName.text = data.productInfo?.category_name
        _binding.tvProductDesc.text = data.productInfo?.name.toString()
        _binding.tvProductPrice.text = Utils.getPrice(data.productInfo?.finalPrice.toString())
        _binding.tvDiscountPrice.text = Utils.getPrice(data.productInfo?.originalPrice.toString())

        isWishList= data.productInfo?.inWhishlist ?:false
        if (data.productInfo?.inWhishlist== true) {
            _binding.ivWishlist.background =
                ResourcesCompat.getDrawable(resources, R.drawable.ic_wishlist, null)
        } else {
            _binding.ivWishlist.background =
                ResourcesCompat.getDrawable(resources, R.drawable.ic_diswishlist, null)
        }
        val variantList = ArrayList<VariantItem>()
        data.variant?.let { variantList.addAll(it) }

        setupVariantAdapter(variantList)
        data.productInstruction?.let { setupInstructionAdapter(it) }
        Log.e("gson", Gson().toJson(variantList))
        callFirstPositionVariantCheck(variantList)

        val imageList = ArrayList<ProductImageItem>()
        data.productImage?.let { imageList.addAll(it) }
        if (imageList.size == 0) {
            _binding.viewPager.hide()
            _binding.vieww.show()
            _binding.rvGallery.hide()
        } else {
            _binding.viewPager.show()
            _binding.vieww.hide()
            _binding.rvGallery.hide()

            adapterSetup(imageList)
            setCurrentIndicator(0)

            setupGalleryAdapter(imageList)

        }
        _binding.ivratting.rating = data.productInfo?.averageRating?.toFloat() ?: 0.0f
        if (data.productReview?.size == 0) {
            _binding.rvReview.hide()

        } else {
            _binding.rvReview.show()
            _binding.rvReview.layoutManager =
                GridLayoutManager(this@ActProductDetail, 1, GridLayoutManager.HORIZONTAL, false)
            val adapter =
                data.productReview?.let {
                    RattingAdapter(this@ActProductDetail, it) { id: String, name: String ->
                    }
                }
            _binding.rvReview.adapter = adapter

        }

        val otherDescriptionArray = data.productInfo?.otherDescriptionArray
        otherDescriptionArray?.removeAll { it.description?.isEmpty() == true }
        if (data.productInfo?.otherDescriptionArray?.size == 0) {
            _binding.rvDescription.hide()
        } else {
            _binding.rvDescription.show()

            _binding.rvDescription.apply {
                layoutManager = LinearLayoutManager(this@ActProductDetail)
                adapter = otherDescriptionArray?.let { DescriptionAdapter(it) }
            }
        }
    }

    private fun setupVariantAdapter(variantList: ArrayList<VariantItem>) {
        variantAdapter = VariantAdapter(variantList, this@ActProductDetail, variantName) {
            val selectedVariantList = ArrayList<String>()
            for (i in 0 until variantList.size) {
                for (j in 0 until variantList[i].dataItem?.size!!) {
                    if (variantList[i].dataItem!![j].isSelect == true) {
                        selectedVariantList.add(variantList[i].dataItem!![j].name?.trim().toString())
                    }
                }
            }
            val filterValue = selectedVariantList.joinToString(separator = "-") { it.replace(" ","") }
            callVariantApi(filterValue, true)
        }

        _binding.rvVariant.apply {
            layoutManager = LinearLayoutManager(this@ActProductDetail)
            adapter = variantAdapter
            isNestedScrollingEnabled = true
        }
    }
    private fun callFirstPositionVariantCheck(variantList: ArrayList<VariantItem>) {
        if (variantList.size != 0) {
            val selectedVariantList = ArrayList<String>()

            for (i in 0 until variantList.size) {
                if (variantList[i].value?.size != 0) {

                    if (variantName.isNotEmpty()) {
                        selectedVariantList.add(variantName.trim())
                        break

                    } else {
                        selectedVariantList.add(variantList[i].value?.get(0)?.trim() ?: "")
                        break

                    }

                }
            }

            val filterValue = selectedVariantList.joinToString(separator = "-") { it.trim() }
            callVariantApi(filterValue, false)
        }
    }
    private fun callVariantApi(variant_sku: String,fromClick:Boolean) {
        if(fromClick)
        {
            Utils.showLoadingProgress(this@ActProductDetail)

        }
        val productDetailRequest = HashMap<String, String>()
        productDetailRequest["product_id"] = intent.getStringExtra(Constants.ProductId) ?: ""
        productDetailRequest["variant_sku"] = variant_sku
        productDetailRequest["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActProductDetail).variantStock(productDetailRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 1) {
                        val data = response.body.data
                        _binding.tvProductPrice.text = Utils.getPrice(data?.finalPrice.toString())
                        _binding.tvCurrency.text = SharePreference.getStringPref(this@ActProductDetail,SharePreference.currency_name).toString()
                        _binding.tvDiscountPrice.text = Utils.getPrice(data?.originalPrice.toString())
                        variant_id = data?.id.toString()
                        disCountPrice = data?.discountPrice.toString()
                        originalPrice = data?.originalPrice.toString()
                        finalPrice = data?.finalPrice.toString()
                        variantName = data?.variant.toString()
                        productStock = data?.stock.toString()


                        if (data?.stock == 0) {

                            _binding.btnAddToCart.text = getString(R.string.notify_me)
                            _binding.tvOutStock.show()

                        } else {

                            _binding.tvOutStock.hide()
                            _binding.btnAddToCart.text = getString(R.string.add_to_cart)

                        }
                    }

                }
                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private val startActivityForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        val resultCode = result.resultCode
        val data = result.data
        when (resultCode) {
            Activity.RESULT_OK -> {
                relatedProductItemList.clear()
                callRelatedProductApi()

            }
        }
    }


    private fun initClickListeners() {


        _binding.btnFeedback.setSafeOnClickListener {
            openFeedbackDialog()
        }

        _binding.ivWishlist.setSafeOnClickListener {
            setResult(RESULT_OK)
            if(isWishList)
            {
                callWishlist("remove",product_id.toInt(),"",0)
            }else
            {
                callWishlist("add",product_id.toInt(),"",0)

            }
        }
        _binding.btnMore.setSafeOnClickListener {
            val contactUs =
                SharePreference.getStringPref(this@ActProductDetail, SharePreference.returnPolicy)
                    .toString()
            val uri: Uri = Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)

            startActivity(intent)
        }
        _binding.ivBack.setSafeOnClickListener {
            finish()
        }
        _binding.clcart.setSafeOnClickListener {
            startActivity(Intent(this@ActProductDetail, ActShoppingCart::class.java))
        }
        _binding.btnAddToCart.setSafeOnClickListener {
            Log.e("Stock", productStock)

            if (productStock == "0") {
                if (Utils.isLogin(this@ActProductDetail)) {
                    val notifyUser = HashMap<String, String>()
                    notifyUser["user_id"] = SharePreference.getStringPref(this@ActProductDetail, SharePreference.userId).toString()
                    notifyUser["product_id"] = product_id
                    notifyUser["theme_id"] = getString(R.string.theme_id)
                    notifyUser(notifyUser)
                } else {
                    val intent = Intent(this@ActProductDetail, ActWelCome::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    finish()
                }
            } else {
                if (Utils.isLogin(this@ActProductDetail)) {
                    val addtocart = HashMap<String, String>()
                    addtocart["user_id"] = SharePreference.getStringPref(this@ActProductDetail, SharePreference.userId).toString()
                    addtocart["product_id"] = product_id
                    addtocart["variant_id"] = variant_id
                    addtocart["qty"] = "1"
                    addtocart["theme_id"] = getString(R.string.theme_id)

                    addtocartApi(
                        addtocart,
                        product_id.toInt(),
                        variant_id.toInt(), 0
                    )
                } else {
                    guestUserAddToCart(FeaturedProductsSub(), product_id.toInt(), false,variant_id)

                }
            }
        }
    }



    private fun openFeedbackDialog() {
        var dialog: Dialog? = null
        try {
            dialog?.dismiss()
            dialog = Dialog(this@ActProductDetail, R.style.AppCompatAlertDialogStyleBig)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
            )
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            val mInflater = LayoutInflater.from(this@ActProductDetail)
            val mView = mInflater.inflate(R.layout.dlg_feedback, null, false)
            val btnCancel: AppCompatButton = mView.findViewById(R.id.btnCancel)
            val btnRateNow: AppCompatButton = mView.findViewById(R.id.btnRateNow)
            val ivStar1: ImageView = mView.findViewById(R.id.ivStar1)
            val ivStar2: ImageView = mView.findViewById(R.id.ivStar2)
            val ivStar3: ImageView = mView.findViewById(R.id.ivStar3)
            val ivStar4: ImageView = mView.findViewById(R.id.ivStar4)
            val ivStar5: ImageView = mView.findViewById(R.id.ivStar5)
            val title: EditText = mView.findViewById(R.id.edTitle)
            val edNote: AppCompatEditText = mView.findViewById(R.id.edNote)
            ivStar1.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
            ivStar2.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
            ivStar3.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
            ivStar4.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
            ivStar5.imageTintList =
                AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
            ivStar1.setSafeOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
                rattingValue = "1"
            }
            ivStar2.setSafeOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
                rattingValue = "2"
            }
            ivStar3.setSafeOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
                rattingValue = "3"
            }
            ivStar4.setSafeOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.star)
                rattingValue = "4"
            }
            ivStar5.setSafeOnClickListener {
                ivStar1.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar2.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar3.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar4.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                ivStar5.imageTintList =
                    AppCompatResources.getColorStateList(this@ActProductDetail, R.color.appcolor)
                rattingValue = "5"
            }
            val finalDialog: Dialog = dialog
            btnRateNow.setSafeOnClickListener {
                when {
                    rattingValue == "0" -> {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            resources.getString(R.string.validation_alert)
                        )
                    }
                    edNote.text?.isEmpty() == true -> {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            resources.getString(R.string.validation_alert)
                        )
                    }
                    title.text?.isEmpty() == true -> {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            resources.getString(R.string.validation_alert)
                        )
                    }
                    else -> {
                        val ratting = HashMap<String, String>()
                        ratting["id"] = intent.getStringExtra(Constants.ProductId) ?: ""
                        ratting["user_id"] =
                            SharePreference.getStringPref(
                                this@ActProductDetail,
                                SharePreference.userId
                            )
                                .toString()
                        ratting["rating_no"] = rattingValue.toString()
                        ratting["title"] = title.text.toString()
                        ratting["description"] = edNote.text.toString()
                        ratting["theme_id"] = getString(R.string.theme_id)

                        callProductRatting(ratting)
                        finalDialog.dismiss()
                    }
                }
            }
            btnCancel.setSafeOnClickListener {
                finalDialog.dismiss()
            }
            dialog.setContentView(mView)
            dialog.show()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun callProductDetailApi() {
        Utils.showLoadingProgress(this@ActProductDetail)
        val productDetailRequest = HashMap<String, String>()
        productDetailRequest["id"] = intent.getStringExtra(Constants.ProductId) ?: ""
        productDetailRequest["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(this@ActProductDetail, SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(this@ActProductDetail)
                        .productDetailGuest(productDetailRequest)
                } else {
                    ApiClient.getClient(this@ActProductDetail).productDetail(productDetailRequest)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 1) {
                        variantName = response.body.data?.productInfo?.defaultVariantName.toString()

                        response.body.data?.let { setupDetailData(it) }
                        productInfo = response.body.data?.productInfo!!
                        product_id = response.body.data.productInfo.id.toString()
                        variant_id = response.body.data.productInfo.defaultVariantId.toString()
                        disCountPrice = response.body.data?.productInfo?.discountPrice.toString()
                        originalPrice = response.body.data?.productInfo?.originalPrice.toString()
                        finalPrice = response.body.data?.productInfo?.finalPrice.toString()
                        _binding.tvReturn.text =
                            response.body.data?.productInfo?.retuen_text.toString()
                        if (response.body.data?.productInfo?.is_review == 0) {
                            if (Utils.isLogin(this@ActProductDetail)) {
                                _binding.btnFeedback.show()
                            } else {
                                _binding.btnFeedback.hide()
                            }
                        } else {
                            _binding.btnFeedback.hide()
                        }
                        productStock = response.body.data.productInfo.productStock.toString()
                        if(response.body.data.variant?.size==0)
                        {
                            if (response.body.data.productInfo.productStock == 0) {

                                _binding.btnAddToCart.text = getString(R.string.notify_me)
                                _binding.tvOutStock.show()
                            } else {
                                _binding.tvOutStock.hide()
                                _binding.btnAddToCart.text = getString(R.string.add_to_cart)
                            }
                        }

                    }


                }
                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    fun setupInstructionAdapter( instructionList:ArrayList<ProductInstruction>)
    {
        _binding.rvInstructions.apply {
            layoutManager=LinearLayoutManager(this@ActProductDetail)
            adapter=InstructionAdapter(instructionList)
            isNestedScrollingEnabled=true
        }
    }

    private fun callProductRatting(ratting: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActProductDetail)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetail)
                .productRating(ratting)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val rattingResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            callProductDetailApi()
                            Utils.successAlert(
                                this@ActProductDetail,
                                rattingResponse?.message.toString()
                            )
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                rattingResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                response.body.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetail,ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun notifyUser(notifyUser: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActProductDetail)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetail)
                .notifyUser(notifyUser)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            //Utils.successAlert(this@ActProductDetails,addtocart?.point.toString())
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                addtocart?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                addtocart?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetail, ActWelCome::class.java))
                            finish()
                            finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        "Something went wrong"
                    )
                }
            }
        }

    }


    private fun adapterSetup(imageList: ArrayList<ProductImageItem>) {
        adapter = ViewPagerAdapter(imageList, this@ActProductDetail)
        _binding.viewPager.adapter = adapter
        _binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(i: Int, v: Float, i1: Int) {}
            override fun onPageSelected(i: Int) {
                setCurrentIndicator(i)
            }

            override fun onPageScrollStateChanged(i: Int) {}
        })
        setupIndicator()
    }

    private fun setupIndicator() {
        _binding.indicatorContainer.removeAllViews()
        val indicators = arrayOfNulls<ImageView>(adapter.count)
        val layoutParams: LinearLayout.LayoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        layoutParams.setMargins(8, 0, 8, 0)
        for (i in indicators.indices) {
            indicators[i] = ImageView(applicationContext)
            indicators[i]?.apply {
                this.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@ActProductDetail,
                        R.drawable.tab_indicator_default
                    )
                )
                this.layoutParams = layoutParams
            }
            _binding.indicatorContainer.addView(indicators[i])
        }
    }


    private fun setCurrentIndicator(index: Int) {
        val childCount = _binding.indicatorContainer.childCount
        for (i in 0 until childCount) {
            val imageview = _binding.indicatorContainer[i] as ImageView
            if (i == index) {
                imageview.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@ActProductDetail,
                        R.drawable.tab_indicator_selected
                    )
                )
            } else {
                imageview.setImageDrawable(
                    ContextCompat.getDrawable(
                        this@ActProductDetail,
                        R.drawable.tab_indicator_default
                    )
                )

            }
        }
    }


    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(this@ActProductDetail)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetail)
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        this@ActProductDetail,
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {

                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                cartQtyResponse?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetail, ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail, response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActProductDetail, "Something went wrong")
                }
            }
        }
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {
        SharePreference.setStringPref(
            this@ActProductDetail,
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            this@ActProductDetail,
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(this@ActProductDetail)
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setSafeOnClickListener {

            startActivity(Intent(this@ActProductDetail, ActShoppingCart::class.java))
        }
        confirmDialogBinding.tvContinueShopping.setSafeOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }


    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?, fromRelatedItem: Boolean,variant_id: String) {
        Log.e("Position", id.toString())
        val cartData = cartData(data, fromRelatedItem)
        Log.e("cartData2", Gson().toJson(cartData))

        val cartListData = SharePreference.getStringPref(
            this@ActProductDetail,
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type

        val cartList = Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))

        if (cartList == null) {
            dlgConfirm(cartData.name + " " + R.string.addsuccess)
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)

            SharePreference.setStringPref(
                this@ActProductDetail,
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                this@ActProductDetail,
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            _binding.tvCount.text = cartDataList.size.toString()
        } else if (isProductAlreadyAdded(cartList, id.toString(),variant_id)) {
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.id.toString(), i, data.defaultVariantId.toString())
                }
            }
        } else {
            dlgConfirm(cartData.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                this@ActProductDetail,
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                this@ActProductDetail,
                SharePreference.cartCount,
                cartList.size.toString()
            )
            _binding.tvCount.text = cartList.size.toString()
        }
    }


    private fun dlgAlreadyCart(product_id: String, i: Int, variant_id: String) {
        val builder = AlertDialog.Builder(this@ActProductDetail)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(this@ActProductDetail)) {
                cartqty["product_id"] = product_id
                cartqty["user_id"] =
                    SharePreference.getStringPref(this@ActProductDetail, SharePreference.userId)
                        .toString()
                cartqty["variant_id"] = variant_id
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = resources.getString(R.string.theme_id)
                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    this@ActProductDetail,
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                val cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                val count = cartList[i].qty!!
                Log.e("Count", count.toString())
                cartList[i].qty = count.plus(1)

                manageOfflineData(cartList)

            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    private fun cartData(data: FeaturedProductsSub, fromRelatedItem: Boolean): ProductListItem {

        return if (fromRelatedItem) {
            ProductListItem(
                data.coverImagePath.toString(),
                data.defaultVariantId.toString(),
                data.finalPrice.toString(),
                data.discountPrice.toString(),
                data.id.toString(),
                1,
                data.name.toString(),
                "",
                "",
                "",
                data.originalPrice.toString()
            )
        } else {
            ProductListItem(
                productInfo.coverImagePath.toString(),
                variant_id,
                finalPrice,
                disCountPrice,
                productInfo.id.toString(),
                1,
                productInfo.name.toString(),
                "",
                "",
                variantName,
                originalPrice
            )
        }
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String,variant_id: String): Boolean {
        return cartList.filter { it.productId == id  && it.variantId==variant_id}.size == 1
    }


    private fun paginationRelatedItems() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callRelatedProductApi()
            }
        }
        _binding.rvRelatedItems.addOnScrollListener(paginationListener)
    }

    private fun setupGalleryAdapter(imageList: ArrayList<ProductImageItem>) {


        _binding.rvGallery.apply {
            layoutManager =
                LinearLayoutManager(this@ActProductDetail, RecyclerView.HORIZONTAL, false)
            adapter = GalleryAdapter(imageList)
            isNestedScrollingEnabled = true
        }
    }

    private fun callRelatedProductApi() {
        Utils.showLoadingProgress(this@ActProductDetail)
        val relatedProductRequest = HashMap<String, String>()
        relatedProductRequest["theme_id"] = getString(R.string.theme_id)
        relatedProductRequest["product_id"] = intent.getStringExtra(Constants.ProductId) ?: ""

        lifecycleScope.launch {

            val response =
                if (SharePreference.getStringPref(this@ActProductDetail, SharePreference.token).isNullOrEmpty()) {
                    ApiClient.getClient(this@ActProductDetail)
                        .relatedProductGuest(currentPage.toString(), relatedProductRequest)
                } else {
                    ApiClient.getClient(this@ActProductDetail)
                        .relatedProduct(currentPage.toString(), relatedProductRequest)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {

                                currentPage = categoriesProductResponse?.currentPage!!.toInt()
                                total_pages = categoriesProductResponse.lastPage!!.toInt()
                                categoriesProductResponse.data?.let {
                                    relatedProductItemList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvRelatedItems.hide()
                            }
                            relatedProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                response?.body?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                response?.body?.message.toString()
                            )
                            Utils.setInvalidToken(this@ActProductDetail)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()

                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()

                    Utils.errorAlert(
                        this@ActProductDetail,
                        getString(R.string.something_went_wrong)
                    )
                }
            }
        }
    }

    private fun relatedProductAdapter(relatedItemList: ArrayList<FeaturedProductsSub>) {
        _binding.rvRelatedItems.layoutManager = manager
        relatedProductAdapter =
            RelatedProductAdapter(this@ActProductDetail, relatedItemList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            this@ActProductDetail,
                            SharePreference.isLogin
                        )
                    ) {
                        if (relatedItemList[i].inWhishlist == false) {
                            callWishlist(
                                "add",
                                relatedItemList[i].id,
                                "RelatedItemProduct",
                                i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                relatedItemList[i].id,
                                "RelatedItemProduct", i
                            )
                        }
                    } else {
                        startActivity(Intent(this@ActProductDetail, ActWelCome::class.java))
                        finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = relatedItemList[i]
                    if (!Utils.isLogin(this@ActProductDetail)) {
                        guestUserAddToCart(data, relatedItemList[i].id, true,relatedItemList[i].defaultVariantId.toString())
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] = SharePreference.getStringPref(
                            this@ActProductDetail,
                            SharePreference.userId
                        ).toString()
                        addtocart["product_id"] = relatedItemList[i].id.toString()
                        addtocart["variant_id"] = relatedItemList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            relatedItemList[i].id,
                            relatedItemList[i].defaultVariantId,
                            i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(this@ActProductDetail, ActProductDetail::class.java)
                    intent.putExtra(Constants.ProductId, relatedItemList[i].id.toString())
                    startActivityForResult.launch(intent)
                }
            }
        _binding.rvRelatedItems.adapter = relatedProductAdapter
    }
    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(this@ActProductDetail)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetail)
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartData = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                this@ActProductDetail,
                                SharePreference.cartCount,
                                cartData?.count.toString()
                            )
                            setCount(cartData?.count)
                        }
                        0 -> {
                            if(response.body.data?.isOutOFStock==1)
                            {
                                Utils.errorAlert(this@ActProductDetail, response.body.data.message.toString()
                                )
                            }else
                            {
                                dlgAlreadyCart(id.toString(), i!!, defaultVariantId.toString())

                            }


                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                cartData?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetail, ActWelCome::class.java))
                            finish()
                            finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(this@ActProductDetail)
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(this@ActProductDetail, SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActProductDetail)
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                when (itemType) {
                                    "RelatedItemProduct" -> {
                                        relatedProductItemList[position].inWhishlist = true
                                        relatedProductAdapter.notifyItemChanged(position)
                                    }else -> {
                                    isWishList = true
                                    _binding.ivWishlist.background =
                                        ResourcesCompat.getDrawable(
                                            resources,
                                            R.drawable.ic_wishlist,
                                            null
                                        )

                                }
                                }
                            } else {
                                when (itemType) {

                                    "RelatedItemProduct" -> {
                                        relatedProductItemList[position].inWhishlist = false
                                        relatedProductAdapter.notifyItemChanged(position)
                                    }else ->
                                {
                                    isWishList = false
                                    _binding.ivWishlist.background =
                                        ResourcesCompat.getDrawable(
                                            resources,
                                            R.drawable.ic_diswishlist,
                                            null
                                        )

                                }

                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActProductDetail,
                                wishlistResponse?.message.toString()
                            )
                            startActivity(Intent(this@ActProductDetail, ActWelCome::class.java))
                            finish()
                            finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActProductDetail)
                    } else {
                        Utils.errorAlert(
                            this@ActProductDetail,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActProductDetail,
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }


}