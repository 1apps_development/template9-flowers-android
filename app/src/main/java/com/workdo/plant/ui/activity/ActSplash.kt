package com.workdo.plant.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import androidx.lifecycle.lifecycleScope

import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class ActSplash : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_splash)


        baseUrl()
        printKeyHash()



    }


    private fun baseUrl() {
        val hashmap = HashMap<String, String>()
        hashmap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.defaultClient.baseUrl(hashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val responseData = response.body
                    when (responseData.status) {
                        1 -> {

                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.BaseUrl,
                                responseData.data?.baseUrl.toString().plus("/")
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.ImageUrl,
                                responseData.data?.imageUrl.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.PaymentUrl,
                                responseData.data?.paymentUrl.toString().plus("/")
                            )
                            getExterUrl()
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActSplash,
                                responseData.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSplash,
                                responseData.message.toString()
                            )
                            startActivity(Intent(this@ActSplash, ActWelCome::class.java))

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActSplash)
                    } else {
                        Utils.errorAlert(
                            this@ActSplash,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.errorAlert(
                        this@ActSplash,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.errorAlert(
                        this@ActSplash,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO exter url api
    private suspend fun getExterUrl() {
        val exterURL = HashMap<String, String>()
        exterURL["theme_id"] = getString(R.string.theme_id)
        withContext(lifecycleScope.coroutineContext) {
            when (val response = ApiClient.getClient(this@ActSplash).extraUrl(exterURL)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val extraUrl = response.body
                    when (response.body.status) {
                        1 -> {
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.Terms,
                                extraUrl.data?.terms.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.Contact_Us,
                                extraUrl.data?.contactUs.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.insta,
                                extraUrl.data?.insta.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.youtube,
                                extraUrl.data?.youtube.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.messanger,
                                extraUrl.data?.messanger.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.twitter,
                                extraUrl.data?.twitter.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActSplash,
                                SharePreference.returnPolicy,
                                extraUrl.data?.returnPolicy.toString()
                            )

                            startActivity(Intent(this@ActSplash, ActMain::class.java))
                            finish()
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActSplash,
                                extraUrl.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActSplash,
                                extraUrl.message.toString()
                            )
                            startActivity(Intent(this@ActSplash, ActWelCome::class.java))
                            finish()

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActSplash)
                    } else {
                        Utils.errorAlert(
                            this@ActSplash,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.errorAlert(
                        this@ActSplash,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.errorAlert(
                        this@ActSplash,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    @SuppressLint("PackageManagerGetSignatures")
    fun printKeyHash(): String? {
        val packageInfo: PackageInfo
        var key: String? = null
        try {
            val packageName = applicationContext.packageName
            packageInfo = packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            Log.e("Package Name=", applicationContext.packageName)
            for (signature in packageInfo.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                key = String(Base64.encode(md.digest(), 0))
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("Name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("No such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("Exception", e.toString())
        }
        return key
    }
}