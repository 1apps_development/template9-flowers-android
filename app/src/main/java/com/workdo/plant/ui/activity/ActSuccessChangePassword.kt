package com.workdo.plant.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import com.workdo.plant.databinding.ActSuccessChangePasswordBinding

class ActSuccessChangePassword : AppCompatActivity() {
    private lateinit var binding: ActSuccessChangePasswordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActSuccessChangePasswordBinding.inflate(layoutInflater)

        setContentView(binding.root)

    }
}