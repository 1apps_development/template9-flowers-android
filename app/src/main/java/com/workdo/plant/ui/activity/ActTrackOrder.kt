package com.workdo.plant.ui.activity

import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi
import com.workdo.plant.R
import com.workdo.plant.base.BaseActivity
import com.workdo.plant.databinding.ActTrackOrderBinding
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener


class ActTrackOrder : BaseActivity() {
    private lateinit var _binding: ActTrackOrderBinding
    var trackOrderStatus = ""

    override fun setLayout(): View = _binding.root

    @RequiresApi(Build.VERSION_CODES.M)
    override fun initView() {
        _binding = ActTrackOrderBinding.inflate(layoutInflater)
        trackOrderStatus = intent.getStringExtra("trackOrderStatus").toString()
        inti()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun inti() {
        _binding.ivBack.setSafeOnClickListener { finish() }
        if (trackOrderStatus=="0"){
            _binding.ivOrderConfirm.setImageResource(R.drawable.ic_option)
            _binding.ivOrderDelivered.setImageResource(R.drawable.ic_unoption)
            _binding.view.setBackgroundColor(getColor(R.color.darkgreen))
        }else if (trackOrderStatus=="1"){
            _binding.ivOrderConfirm.setImageResource(R.drawable.ic_option)
            _binding.ivOrderDelivered.setImageResource(R.drawable.ic_option)
            _binding.view.setBackgroundColor(getColor(R.color.darkgreen))
        }
    }
}