package com.workdo.plant.ui.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.ActChangePasswordBinding
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import kotlinx.coroutines.launch

class ActChangePassword : AppCompatActivity() {
    private lateinit var _binding: ActChangePasswordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding= ActChangePasswordBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        init()
    }

    private fun init() {
        _binding.ivBack.setSafeOnClickListener { finish() }
        _binding.btnChangePassword.setSafeOnClickListener {
            when {
                _binding.edPassword.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActChangePassword,
                        resources.getString(R.string.validation_password_)
                    )
                }
                _binding.edConfirmPassword.text?.isEmpty() == true -> {
                    Utils.errorAlert(
                        this@ActChangePassword,
                        resources.getString(R.string.validation_confirm_password)
                    )
                }
                _binding.edPassword.text.toString() != _binding.edConfirmPassword.text.toString() -> {
                    Utils.errorAlert(
                        this@ActChangePassword,
                        resources.getString(R.string.validation_valid_password)
                    )
                }
                else -> {
                    callChangePasswordApi()
                }
            }
        }
    }

    //TODO Change Password api calling
    private fun callChangePasswordApi() {
        val request = HashMap<String, String>()
        request["user_id"] =
            SharePreference.getStringPref(this@ActChangePassword, SharePreference.userId).toString()
        request["password"] = _binding.edPassword.text.toString()
        request["theme_id"] =getString(R.string.theme_id)
        Utils.showLoadingProgress(this@ActChangePassword)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActChangePassword).setChangePassword(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val changesPasswordResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            finish()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActChangePassword,
                                response.body.data?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActChangePassword,
                                response.body.message.toString()
                            )
                            startActivity(Intent(this@ActChangePassword,ActWelCome::class.java))
                        }
                    }
                }
                is NetworkResponse.ApiError -> {
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActChangePassword)
                    } else {
                        Utils.errorAlert(
                            this@ActChangePassword,
                            response.body.message.toString()
                        )
                    }
                }
                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActChangePassword,
                        resources.getString(R.string.internet_connection_error)
                    )
                }
                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActChangePassword,
                        "Something went wrong"
                    )
                }
            }
        }
    }
}