package com.workdo.plant.ui.authentication

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.ActForgotPasswordBinding
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import kotlinx.coroutines.launch


class ActForgotPassword : AppCompatActivity() {
    private lateinit var _binding: ActForgotPasswordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding= ActForgotPasswordBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        init()
    }

    private fun init(){
        _binding.btnSendCode.setSafeOnClickListener {
            when {
                _binding.edEmailAddress.text?.isEmpty() == true -> {
                    Utils.errorAlert(this@ActForgotPassword, resources.getString(R.string.validation_email))
                }
                !Utils.isValidEmail(_binding.edEmailAddress.text.toString()) -> {
                    Utils.errorAlert(this@ActForgotPassword, resources.getString(R.string.validation_valid_email))
                }
                else -> {
                    val forgotPasswordOtpApi = HashMap<String, String>()
                    forgotPasswordOtpApi["email"] = _binding.edEmailAddress.text.toString()
                    forgotPasswordOtpApi["theme_id"]= getString(R.string.theme_id)
                    callForgotPasswordSendOpt(forgotPasswordOtpApi)
                }
            }
        }

        _binding.linearContacts.setSafeOnClickListener {
            val contactUs= SharePreference.getStringPref(this@ActForgotPassword, SharePreference.Contact_Us).toString()
            val uri: Uri = Uri.parse(contactUs)
            Log.e("uri",uri.toString())
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)

            startActivity(intent)
        }
    }

    //TODO send otp api
    private fun callForgotPasswordSendOpt(forgotPasswordOtpApi: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActForgotPassword)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActForgotPassword).setforgotpasswordsendotp(forgotPasswordOtpApi)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val forgotPasswordOtpResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            startActivity(
                                Intent(
                                    this@ActForgotPassword,
                                    ActAuthentication::class.java
                                ).putExtra(
                                    "email",
                                    _binding.edEmailAddress.text.toString()
                                )
                            )
                        }

                        0 -> {
                            Utils.errorAlert(this@ActForgotPassword, forgotPasswordOtpResponse?.message.toString())
                        }
                        9->{
                            Utils.errorAlert(this@ActForgotPassword, forgotPasswordOtpResponse?.message.toString())
                            startActivity(Intent(this@ActForgotPassword,ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActForgotPassword)
                    }else{
                        Utils.errorAlert(this@ActForgotPassword, response.body.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActForgotPassword, resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActForgotPassword,"Something went wrong")
                }
            }
        }
    }
}