package com.workdo.plant.ui.authentication

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.lifecycle.lifecycleScope
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.base.BaseActivity
import com.workdo.plant.databinding.ActForgotpasswordsaveBinding
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener

import kotlinx.coroutines.launch

class ActForgotPasswordSave : BaseActivity() {

    private lateinit var _binding: ActForgotpasswordsaveBinding
    var strEmail: String = ""
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActForgotpasswordsaveBinding.inflate(layoutInflater)
        strEmail = intent.getStringExtra("email") ?: ""
        init()
    }

    private fun init() {
        _binding.ivBack.setSafeOnClickListener { finish() }
        _binding.btnSubmit.setSafeOnClickListener {
            when {
                _binding.edNewPassword.text?.isEmpty() == true -> {
                    Utils.errorAlert(this@ActForgotPasswordSave, resources.getString(R.string.validation_password_))
                }
                _binding.edConfirmPassword.text?.isEmpty() == true -> {
                    Utils.errorAlert(this@ActForgotPasswordSave, resources.getString(R.string.validation_confirm_password))
                }
                _binding.edNewPassword.text.toString() != _binding.edConfirmPassword.text.toString() -> {
                    Utils.errorAlert(this@ActForgotPasswordSave, resources.getString(R.string.validation_valid_password))
                }
                else -> {
                    val forgotPasswordSave = HashMap<String, String>()
                    forgotPasswordSave["email"] = strEmail
                    forgotPasswordSave["password"] = _binding.edNewPassword.text.toString()
                    forgotPasswordSave["theme_id"]= getString(R.string.theme_id)
                    callForgotPasswordSaveApi(forgotPasswordSave)
                }
            }
        }
        _binding.tvContactUs.setSafeOnClickListener {
            val contactUs = SharePreference.getStringPref(this@ActForgotPasswordSave, SharePreference.Contact_Us).toString()
            val uri: Uri = Uri.parse(contactUs)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.addCategory(Intent.CATEGORY_BROWSABLE)
            startActivity(intent)
        }
    }

    //TODO change password api
    private fun callForgotPasswordSaveApi(forgotPasswordSave: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActForgotPasswordSave)
        lifecycleScope.launch {

            when (val response = ApiClient.getClient(this@ActForgotPasswordSave)
                .setforgotpasswordsave(forgotPasswordSave)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val forgotpasswordsave = response.body.data
                    when (response.body.status) {
                        1 -> {
                            startActivity(
                                Intent(
                                    this@ActForgotPasswordSave,
                                    ActLogin::class.java
                                )
                            )
                        }

                        0 -> {
                            Utils.errorAlert(
                                this@ActForgotPasswordSave,
                                forgotpasswordsave?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActForgotPasswordSave,
                                forgotpasswordsave?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActForgotPasswordSave)
                    } else {
                        Utils.errorAlert(
                            this@ActForgotPasswordSave,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActForgotPasswordSave,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActForgotPasswordSave,
                        "Something went wrong"
                    )
                }
            }
        }
    }
}