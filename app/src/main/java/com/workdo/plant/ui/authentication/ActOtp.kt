package com.workdo.plant.ui.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.workdo.plant.databinding.ActOtpBinding

class ActOtp : AppCompatActivity() {
    private lateinit var binding: ActOtpBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActOtpBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}