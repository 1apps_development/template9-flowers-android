package com.workdo.plant.ui.authentication

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.ActRegisterBinding
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.activity.ActMain
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.launch

class ActRegister : AppCompatActivity() {
    private lateinit var _binding: ActRegisterBinding
    var token = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding= ActRegisterBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        init()
    }



    private fun init() {
        FirebaseApp.initializeApp(this@ActRegister)
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    println("Failed to get token")
                    return@OnCompleteListener
                }
                token = task.result
                Log.d("Token-->", token)
            })
        _binding.tvLogin.setSafeOnClickListener {
            startActivity(Intent(this@ActRegister,ActLoginOption::class.java))
        }
        _binding.btnRegister.setSafeOnClickListener {
            if (_binding.edFirstName.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_f_name))
            } else if (_binding.edLastName.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_l_name))
            } else if (_binding.edEmailAddress.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_email))
            } else if (!Utils.isValidEmail(_binding.edEmailAddress.text.toString())) {
                Utils.errorAlert(this, resources.getString(R.string.validation_valid_email))
            } else if (_binding.edPhoneNumber.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_phone_number))
            } else if (_binding.edPassword.text.isNullOrEmpty()) {
                Utils.errorAlert(this, resources.getString(R.string.validation_password_))
            } else {
                val signUpRequest = HashMap<String, String>()
                signUpRequest["first_name"] = _binding.edFirstName.text.toString()
                signUpRequest["last_name"] = _binding.edLastName.text.toString()
                signUpRequest["email"] = _binding.edEmailAddress.text.toString()
                signUpRequest["password"] = _binding.edPassword.text.toString()
                signUpRequest["mobile"] = _binding.edPhoneNumber.text.toString()
                signUpRequest["device_type"] = "android"
                signUpRequest["register_type"] = "email"
                signUpRequest["token"] = token
                signUpRequest["theme_id"] = getString(R.string.theme_id)
                callRegisterApi(signUpRequest)
            }
        }
    }

    //TODO register api
    private fun callRegisterApi(signUpRequest: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActRegister)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActRegister).setRegistration(signUpRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val registerResponse = response.body
                    val status = response.body.status
                    when (response.body.status) {
                        1 -> {
                            SharePreference.setBooleanPref(
                                this@ActRegister,
                                SharePreference.isLogin,
                                true
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userId,
                                registerResponse.data?.id.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userName,
                                registerResponse.data?.name.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userFirstName,
                                registerResponse.data?.firstName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userProfile,
                                registerResponse.data?.image.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userLastName,
                                registerResponse.data?.lastName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userEmail,
                                registerResponse.data?.email.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.userMobile,
                                registerResponse.data?.mobile.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.token,
                                registerResponse.data?.token.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegister,
                                SharePreference.tokenType,
                                registerResponse.data?.tokenType.toString()
                            )
                            startActivity(
                                Intent(
                                    this@ActRegister,
                                    ActMain::class.java
                                )
                            )
                        }
                        0 -> {
                            Utils.successAlert(
                                this@ActRegister,
                                registerResponse.data?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(this@ActRegister, registerResponse?.message.toString())
                            startActivity(
                                Intent(
                                    this@ActRegister,
                                    ActWelCome::class.java
                                )
                            )
                        }
                    }
                }
                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActRegister)
                    } else {
                        Utils.errorAlert(
                            this@ActRegister,
                            response.body.message.toString()
                        )
                    }
                }
                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActRegister,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActRegister,
                        "Something went wrong"
                    )
                }
            }
        }
    }



}