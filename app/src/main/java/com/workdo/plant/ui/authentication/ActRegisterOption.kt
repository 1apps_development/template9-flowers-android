package com.workdo.plant.ui.authentication

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.ActSignUpOptionBinding
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.activity.ActMain
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject

class ActRegisterOption : AppCompatActivity() {
    //:::::::::::::::Google Login::::::::::::::::://
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private val RC_SIGN_IN = 1
    private var callbackManager: CallbackManager? = null
    var token = ""
    private lateinit var _binding: ActSignUpOptionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActSignUpOptionBinding.inflate(layoutInflater)
        setContentView(_binding.root)
        init()

        initClickListeners()
    }


    private fun init() {
        FirebaseApp.initializeApp(this@ActRegisterOption)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                println("Failed to get token")
                return@OnCompleteListener
            }
            token = task.result
            Log.d("Token-->", token)

        })
        Log.d("Token-->", token)
        Utils.getLog("Token== ", token)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)


        //::::::::::::::Facebook Login::::::::::::::::://
        FacebookSdk.setApplicationId(resources.getString(R.string.facebook_id));
        FacebookSdk.sdkInitialize(this@ActRegisterOption)
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    updateFacebookUI(loginResult)
                }

                override fun onCancel() {}
                override fun onError(error: FacebookException) {
                    Toast.makeText(applicationContext, "" + error.message, Toast.LENGTH_LONG)
                        .show()
                }
            })
    }

    private fun initClickListeners() {
        _binding.tvLogin.setSafeOnClickListener {
            startActivity(Intent(this@ActRegisterOption, ActLoginOption::class.java))
        }
        _binding.btnSignUpWithEmail.setSafeOnClickListener {
            startActivity(Intent(this@ActRegisterOption, ActRegister::class.java))

        }

        _binding.btnGoogleSignUp.setSafeOnClickListener {
            if (Utils.isCheckNetwork(this@ActRegisterOption)) {
                mGoogleSignInClient!!.signOut()
                    .addOnCompleteListener(
                        this
                    ) { signInGoogle() }
            } else {
                Utils.errorAlert(
                    this@ActRegisterOption,
                    resources.getString(R.string.internet_connection_error)
                )
            }
        }

        _binding.btnFaceBookSignUp.setSafeOnClickListener {
            if (AccessToken.getCurrentAccessToken() != null) {
                LoginManager.getInstance().logOut()
            }
            LoginManager
                .getInstance()
                .logInWithReadPermissions(
                    this,
                    getFacebookPermissions()
                )
        }
    }


    //Google
    private fun signInGoogle() {
        val signInIntent = mGoogleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount = completedTask.getResult(ApiException::class.java)!!
            nextGmailActivity(account)
        } catch (e: ApiException) {
            Log.e("Google Login", "signInResult:failed code=" + e.statusCode)
        }
    }

    @SuppressLint("HardwareIds")
    private fun nextGmailActivity(profile: GoogleSignInAccount?) {
        if (profile != null) {
            val loginType = "google"
            val firstName = profile.displayName
            val profileEmail = profile.email
            val profileId = profile.id
            val signUpRequest = HashMap<String, String>()
            signUpRequest["first_name"] = firstName.toString()
            signUpRequest["last_name"] = firstName.toString()
            signUpRequest["email"] = profileEmail.toString()
            signUpRequest["mobile"] = ""
            signUpRequest["device_type"] = "android"
            signUpRequest["google_id"] = profileId.toString()
            signUpRequest["register_type"] = loginType
            signUpRequest["token"] = token
            signUpRequest["theme_id"] = getString(R.string.theme_id)
            callRegisterApi(signUpRequest)
        }
    }


    //Facebook
    private fun getFacebookPermissions(): List<String> {
        return listOf("email")
    }

    //::::::::::::::FacebookLogin:::::::::::::://
    private fun updateFacebookUI(loginResult: LoginResult) {
        val request = GraphRequest.newMeRequest(
            loginResult.accessToken
        ) { `object`, response -> `object`?.let { getFacebookData(it) } }
        val parameters = Bundle()
        parameters.putString(
            "fields",
            "id, first_name, last_name, email,age_range, gender, birthday, location"
        ) // Parámetros que pedimos a facebook
        request.parameters = parameters
        request.executeAsync()
    }

    private fun getFacebookData(`object`: JSONObject) {
        try {
            val profileId = `object`.getString("id")
            var firstName = ""
            var lastname = ""
            if (`object`.has("first_name")) {
                firstName = `object`.getString("first_name")
            }
            if (`object`.has("last_name")) {
                lastname = " " + `object`.getString("last_name")
            }
            var profileEmail = ""
            if (`object`.has("email")) {
                profileEmail = `object`.getString("email")
            }
            val loginType = "facebook"
            val signUpRequest = HashMap<String, String>()
            signUpRequest["first_name"] = firstName.toString()
            signUpRequest["last_name"] = lastname.toString()
            signUpRequest["email"] = profileEmail.toString()
            signUpRequest["mobile"] = ""
            signUpRequest["device_type"] = "android"
            signUpRequest["facebook_id"] = profileId.toString()
            signUpRequest["register_type"] = loginType
            signUpRequest["token"] = token
            signUpRequest["theme_id"] = getString(R.string.theme_id)
            callRegisterApi(signUpRequest)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    //TODO Register api
    private fun callRegisterApi(signUpRequest: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActRegisterOption)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActRegisterOption).setRegistration(signUpRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val registerResponse = response.body
                    val status = response.body.status
                    when (response.body.status) {
                        1 -> {
                            SharePreference.setBooleanPref(
                                this@ActRegisterOption,
                                SharePreference.isLogin,
                                true
                            )
                            SharePreference.setStringPref(
                                this@ActRegisterOption,
                                SharePreference.userId,
                                registerResponse.data?.id.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegisterOption,
                                SharePreference.userName,
                                registerResponse.data?.name.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegisterOption,
                                SharePreference.userFirstName,
                                registerResponse.data?.firstName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegisterOption,
                                SharePreference.userLastName,
                                registerResponse.data?.lastName.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegisterOption,
                                SharePreference.userEmail,
                                registerResponse.data?.email.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegisterOption,
                                SharePreference.userProfile,
                                registerResponse.data?.image.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegisterOption,
                                SharePreference.userMobile,
                                registerResponse.data?.mobile.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegisterOption,
                                SharePreference.token,
                                registerResponse.data?.token.toString()
                            )
                            SharePreference.setStringPref(
                                this@ActRegisterOption,
                                SharePreference.tokenType,
                                registerResponse.data?.tokenType.toString()
                            )
                            startActivity(
                                Intent(
                                    this@ActRegisterOption,
                                    ActMain::class.java
                                )
                            )
                        }
                        0 -> {
                            Utils.successAlert(
                                this@ActRegisterOption,
                                registerResponse.data?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.successAlert(
                                this@ActRegisterOption,
                                registerResponse.data?.message.toString()
                            )
                            startActivity(Intent(this@ActRegisterOption, ActWelCome::class.java))
                        }
                    }
                }
                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(this@ActRegisterOption)
                    } else {
                        Utils.errorAlert(
                            this@ActRegisterOption,
                            response.body.message.toString()
                        )
                    }
                }
                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActRegisterOption,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActRegisterOption,
                        "Something went wrong"
                    )
                }
            }
        }
    }
}