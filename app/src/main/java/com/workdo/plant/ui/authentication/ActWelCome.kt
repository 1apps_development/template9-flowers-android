package com.workdo.plant.ui.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.workdo.plant.databinding.ActIntroBinding
import com.workdo.plant.ui.activity.ActMain
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener

class ActWelCome : AppCompatActivity() {
    private lateinit var binding: ActIntroBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActIntroBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initClickListeners()
    }

    private fun initClickListeners()
    {
        binding.btnLogin.setSafeOnClickListener {
            startActivity(Intent(this@ActWelCome, ActLoginOption::class.java))

        }

        binding.btnSignUp.setSafeOnClickListener {
            startActivity(Intent(this@ActWelCome, ActRegisterOption::class.java))
        }

        binding.btnContinueAsGuest.setSafeOnClickListener {
            startActivity(
                Intent(
                    this@ActWelCome,
                    ActMain::class.java
                )
            )
            finish()
        }
    }
}