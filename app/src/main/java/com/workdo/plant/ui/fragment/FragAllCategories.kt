package com.workdo.plant.ui.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.plant.R
import com.workdo.plant.adapter.AllCategoryAdapter
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.FragCategoriesBinding
import com.workdo.plant.model.HomeCategoriesItem
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.activity.ActCategoryProduct
import com.workdo.plant.ui.activity.ActShoppingCart
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.PaginationScrollListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.bumptech.glide.Glide
import kotlinx.coroutines.launch

class FragAllCategories : Fragment() {
 private lateinit var _binding:FragCategoriesBinding
    private var managerAllCategories: GridLayoutManager? = null
    private var homeCategoriesList = ArrayList<HomeCategoriesItem>()
    private lateinit var allCategoriesAdapter: AllCategoryAdapter
    internal var isLoadingCategorirs = false
    internal var isLastPageCategorirs = false

    private var currentPageCategorirs = 1
    private var total_pagesCategorirs: Int = 0
    var arraysize = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding= FragCategoriesBinding.inflate(layoutInflater)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    fun showLayout() {
        if (_binding.clRound.visibility==View.GONE)
        {
            _binding.clRound.visibility=View.VISIBLE
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun init() {
        paginationCategories()
        managerAllCategories = GridLayoutManager(requireActivity(), 1, GridLayoutManager.VERTICAL, false)
        _binding.clRound.show()
        _binding.tvCount.text = SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
        _binding.clcart.setSafeOnClickListener {
            startActivity(Intent(requireActivity(),ActShoppingCart::class.java))
        }
        _binding.scrollView.viewTreeObserver?.addOnScrollChangedListener {
            val view = _binding.scrollView.getChildAt(_binding.scrollView.childCount - 1)
            val diff = view.bottom - (_binding.scrollView.height + _binding.scrollView.scrollY)
            if (diff == 0) {

                _binding.clRound.hide()
            }
        }
    }

    //TODO adapter set Categorirs
    private fun categoriesAdapter(homeCategoriesList: ArrayList<HomeCategoriesItem>) {
        _binding.rvAllCategories.layoutManager = managerAllCategories
        allCategoriesAdapter =
            AllCategoryAdapter(requireActivity(), homeCategoriesList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    startActivity(Intent(requireActivity(), ActCategoryProduct::class.java).putExtra("maincategory_id", homeCategoriesList[i].categoryId.toString()))
                }
            }
        _binding.rvAllCategories.adapter = allCategoriesAdapter
    }

    //TODO All Categories pagination
    private fun paginationCategories() {

        val paginationListener = object : PaginationScrollListener(managerAllCategories) {
            override fun isLastPage(): Boolean {
                return isLastPageCategorirs
            }

            override fun isLoading(): Boolean {
                return isLoadingCategorirs
            }

            override fun loadMoreItems() {
                isLoadingCategorirs = true
                currentPageCategorirs++
                callCategories()
            }
        }
        _binding.rvAllCategories.addOnScrollListener(paginationListener)
    }


    //TODO  Categories api callig
    private fun callCategories() {
        Utils.showLoadingProgress(requireActivity())
        val categorieshashmap = HashMap<String, String>()
        categorieshashmap["theme_id"] =getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setHomeCategorys(currentPageCategorirs.toString(),categorieshashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                arraysize = categoriesResponse?.data?.size.toString()
                                setRoundCat(categoriesResponse?.data, arraysize)
                                Log.e("LISTSZIE", arraysize)
                                _binding.rvAllCategories.show()
                                _binding.clRound.show()
                                currentPageCategorirs = categoriesResponse?.currentPage ?:0
                                total_pagesCategorirs = categoriesResponse?.lastPage ?:0
                                categoriesResponse?.data?.let {
                                    homeCategoriesList.addAll(it)
                                }
                                if (currentPageCategorirs >= total_pagesCategorirs) {
                                    isLastPageCategorirs = true
                                }
                                isLoadingCategorirs = false
                            } else {
                                _binding.rvAllCategories.hide()
                                _binding.clRound.hide()
                            }
                            allCategoriesAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(),ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }



    //TODO set round menu
    private fun setRoundCat(data: ArrayList<HomeCategoriesItem>?, arraysize: String) {
        if (arraysize == "1") {
            _binding.clCat1.hide()
            _binding.clCat2.hide()
            _binding.clCat3.hide()
            _binding.clCat4.hide()
            _binding.clCat5.show()
            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                .into(_binding.ivCat5)
            _binding.tvCat5.text = data?.get(0)?.name

            _binding.clCat5.setSafeOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(0)?.categoryId.toString())
                )
            }
        } else if (arraysize == "2") {
            _binding.clCat1.show()
            _binding.clCat2.hide()
            _binding.clCat3.hide()
            _binding.clCat4.show()
            _binding.clCat5.hide()
            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                .into(_binding.ivCat1)
            _binding.tvCat1.text = data?.get(0)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(1)?.iconPath.toString()))
                .into(_binding.ivCat4)
            _binding.tvCat4.text = data?.get(1)?.name

            _binding.clCat1.setSafeOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(0)?.categoryId.toString())
                )
            }
            _binding.clCat4.setSafeOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(1)?.categoryId.toString())
                )
            }
        } else if (arraysize == "3") {
            _binding.clCat1.show()
            _binding.clCat2.hide()
            _binding.clCat3.hide()
            _binding.clCat4.show()
            _binding.clCat5.show()
            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                .into(_binding.ivCat1)
            _binding.tvCat1.text = data?.get(0)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(1)?.iconPath.toString()))
                .into(_binding.ivCat5)
            _binding.tvCat5.text = data?.get(1)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(2)?.iconPath.toString()))
                .into(_binding.ivCat4)
            _binding.tvCat4.text = data?.get(2)?.name

            _binding.clCat1.setSafeOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(0)?.categoryId.toString())
                )
            }
            _binding.clCat5.setSafeOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(1)?.categoryId.toString())
                )
            }
            _binding.clCat4.setSafeOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(2)?.categoryId.toString())
                )
            }
        } else if (arraysize == "4") {
            _binding.clCat1.show()
            _binding.clCat2.show()
            _binding.clCat3.show()
            _binding.clCat4.show()
            _binding.clCat5.hide()
            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                .into(_binding.ivCat1)
            _binding.tvCat1.text = data?.get(0)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(1)?.iconPath.toString()))
                .into(_binding.ivCat2)
            _binding.tvCat2.text = data?.get(1)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(2)?.iconPath.toString()))
                .into(_binding.ivCat3)
            _binding.tvCat3.text = data?.get(2)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(3)?.iconPath.toString()))
                .into(_binding.ivCat4)
            _binding.tvCat4.text = data?.get(3)?.name

            _binding.clCat1.setSafeOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(0)?.categoryId.toString())
                )
            }
            _binding.clCat2.setSafeOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(1)?.categoryId.toString())
                )
            }
            _binding.clCat3.setSafeOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(2)?.categoryId.toString())
                )
            }
            _binding.clCat4.setSafeOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(3)?.categoryId.toString())
                )
            }
        } else {
            _binding.clCat1.show()
            _binding.clCat2.show()
            _binding.clCat3.show()
            _binding.clCat4.show()
            _binding.clCat5.hide()
            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                .into(_binding.ivCat1)
            _binding.tvCat1.text = data?.get(0)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(1)?.iconPath.toString()))
                .into(_binding.ivCat2)
            _binding.tvCat2.text = data?.get(1)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(2)?.iconPath.toString()))
                .into(_binding.ivCat3)
            _binding.tvCat3.text = data?.get(2)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(3)?.iconPath.toString()))
                .into(_binding.ivCat4)
            _binding.tvCat4.text = data?.get(3)?.name
            _binding.clCat1.setSafeOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(0)?.categoryId.toString())
                )
            }
            _binding.clCat2.setSafeOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(1)?.categoryId.toString())
                )
            }
            _binding.clCat3.setSafeOnClickListener {
                startActivity(Intent(requireActivity(), ActCategoryProduct::class.java).putExtra("maincategory_id", data?.get(2)?.categoryId.toString()))
            }
            _binding.clCat4.setSafeOnClickListener {
                startActivity(Intent(requireActivity(), ActCategoryProduct::class.java).putExtra("maincategory_id", data?.get(3)?.categoryId.toString())
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()
        currentPageCategorirs = 1
        homeCategoriesList.clear()
        categoriesAdapter(homeCategoriesList)
        callCategories()
        _binding.tvCount.text = SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
    }




}