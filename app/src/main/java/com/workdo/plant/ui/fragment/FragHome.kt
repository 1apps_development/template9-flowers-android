package com.workdo.plant.ui.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.workdo.plant.R
import com.workdo.plant.adapter.BestsellerAdapter
import com.workdo.plant.adapter.RelatedProductAdapter
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.DlgConfirmBinding
import com.workdo.plant.databinding.FragHomeBinding
import com.workdo.plant.model.FeaturedProductsSub
import com.workdo.plant.model.ProductListItem
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.activity.*
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.PaginationScrollListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class FragHome : Fragment() {
    private lateinit var _binding: FragHomeBinding

    private var managerBestsellersSecond: GridLayoutManager? = null
    private var managerBestsellers: GridLayoutManager? = null
    private var bestsellersList = ArrayList<FeaturedProductsSub>()
    private lateinit var bestSellersAdapter: BestsellerAdapter
    private lateinit var trendingProductAdapter: RelatedProductAdapter


    private var trendingProductList = ArrayList<FeaturedProductsSub>()

    internal var isLoadingBestSeller = false
    internal var isLastPageBestSeller = false
    private var currentPageBestSeller = 1
    private var totalPagesBestSeller: Int = 0


    internal var isLoadingTrending = false
    internal var isLastPageTrending = false
    private var currentPageTrending = 1
    private var totalPagesTrending: Int = 0

    var count = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragHomeBinding.inflate(layoutInflater)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()

    }


    @SuppressLint("NotifyDataSetChanged")
    private fun init() {
        _binding.ivCart.setSafeOnClickListener {

            startActivity(Intent(requireActivity(), ActShoppingCart::class.java))
        }
        _binding.ivMenu.setSafeOnClickListener {
            startActivity(Intent(requireActivity(), ActMenu::class.java))

        }
        _binding.btnCheckMore.setSafeOnClickListener {
           requireActivity().supportFragmentManager.beginTransaction().apply {
                replace(R.id.mainContainer, FragProduct())
                commit()
            }
        }
        _binding.clSearch.setSafeOnClickListener {
            startActivity(
                Intent(requireActivity(), ActSearch::class.java).putExtra(
                    "Searched",
                    ""
                )
            )
        }

        _binding.edSearch.setSafeOnClickListener {
            _binding.clSearch.performClick()
        }
        paginationBestSeller()
        paginationTrendingProducts()
        managerBestsellers =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        managerBestsellersSecond =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)

        if (SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                .isNullOrEmpty()
        ) {
            SharePreference.setStringPref(requireActivity(), SharePreference.cartCount, "0")
        }

    }

    private fun paginationBestSeller() {
        val paginationListener = object : PaginationScrollListener(managerBestsellers) {
            override fun isLastPage(): Boolean {
                return isLastPageBestSeller
            }

            override fun isLoading(): Boolean {
                return isLoadingBestSeller
            }

            override fun loadMoreItems() {
                isLoadingBestSeller = true
                currentPageBestSeller++
                callBestseller()
            }
        }
        _binding.rvBestsellers.addOnScrollListener(paginationListener)
    }

    //Adapter set Best seller
    private fun trendingProductAdapter(bestsellersList: ArrayList<FeaturedProductsSub>) {
        _binding.rvTrendingProduct.layoutManager = managerBestsellersSecond
        trendingProductAdapter =
            RelatedProductAdapter(requireActivity(), bestsellersList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (bestsellersList[i].inWhishlist == false) {
                            callWishlist(
                                "add", bestsellersList[i].id, "TrendingList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                bestsellersList[i].id,
                                "TrendingList",
                                i
                            )
                        }
                    } else {
                        startActivity(Intent(requireActivity(), ActWelCome::class.java))
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = bestsellersList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, bestsellersList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = bestsellersList[i].id.toString()
                        addtocart["variant_id"] = bestsellersList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            bestsellersList[i].id,
                            bestsellersList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetail::class.java)
                    intent.putExtra(Constants.ProductId, bestsellersList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvTrendingProduct.adapter = trendingProductAdapter
    }

    private fun paginationTrendingProducts() {
        val paginationListener = object : PaginationScrollListener(managerBestsellersSecond) {
            override fun isLastPage(): Boolean {
                return isLastPageTrending
            }

            override fun isLoading(): Boolean {
                return isLoadingTrending
            }

            override fun loadMoreItems() {
                isLoadingTrending = true
                currentPageTrending++
                callTrendingProducts()
            }
        }
        _binding.rvTrendingProduct.addOnScrollListener(paginationListener)
    }


    private fun callTrendingProducts() {
        Utils.showLoadingProgress(requireActivity())
        val request = HashMap<String, String>()
        request["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {

            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token).isNullOrEmpty()) {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProductGuest(currentPageTrending.toString(), request)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProduct(currentPageTrending.toString(), request)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val trendingResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {


                                _binding.rvTrendingProduct.show()
                                _binding.view.hide()

                                currentPageTrending =
                                    trendingResponse?.currentPage!!.toInt()
                                totalPagesTrending = trendingResponse.lastPage!!.toInt()
                                trendingResponse.data?.let {
                                    trendingProductList.addAll(it)
                                }

                                if (currentPageTrending >= currentPageTrending) {
                                    isLastPageTrending = true
                                }
                                isLoadingTrending = false
                            } else {
                                _binding.rvTrendingProduct.hide()
                                _binding.view.show()
                            }
                            trendingProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingResponse?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToken(requireActivity())
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }

    //TODO Currency Api calling
    private fun callCurrencyApi() {
        Utils.showLoadingProgress(requireActivity())
        val currencyMap = HashMap<String, String>()
        currencyMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setcurrency(currencyMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val currencyResponse = response.body.data
                    when (response.body.status) {
                        1 -> {

                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.currency,
                                currencyResponse?.currency.toString()
                            )
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.currency_name,
                                currencyResponse?.currency_name.toString()
                            )
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                currencyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            // openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
                else -> {}
            }
        }
    }

    //TODO Header content api calling
    private fun callHeaderContentApi() {
        Utils.showLoadingProgress(requireActivity())
        val theme = HashMap<String, String>()
        theme["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setLandingPage(theme)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val headerContenResponse = response.body
                    when (response.body.status) {
                        1 -> {
                            val themeJson = headerContenResponse.data?.themJson

                            val imageList = ArrayList<SlideModel>()

                            for (i in 0 until themeJson?.homepageHeader?.homepageHeaderBgImg?.size!!) {
                                val slideModel = SlideModel(ApiClient.ImageURL.BASE_URL.plus(themeJson.homepageHeader.homepageHeaderBgImg[i]))
                                imageList.add(slideModel)
                            }
                            _binding.imageSlider.setImageList(imageList,ScaleTypes.CENTER_CROP)

                            _binding.tvHeaderTitle.text =
                                themeJson?.homepageHeader?.homepageHeaderHeading
                            _binding.tvSubHeading.text = themeJson?.homepageHeader?.homepageHeader
                            _binding.tvHeaderDescription.text =
                                themeJson?.homepageHeader?.homepageHeaderSubText
                            _binding.btnCheckMore.text =
                                themeJson?.homepageHeader?.homepageHeaderBtnText
                            _binding.tvBestSellerTitle.text =
                                themeJson?.homepageBestseller?.homepageBestsellerHeading



                            _binding.tvSliderTitle.text =
                                themeJson?.homepageSlider?.homepageSliderHeading
                            _binding.tvSliderHeader.text =
                                themeJson?.homepageSlider?.homepageSliderTitleText
                            _binding.tvSliderSubText.text =
                                themeJson?.homepageSlider?.homepageSliderSubText


                         /*   Glide.with(requireActivity())
                                .load(ApiClient.ImageURL.BASE_URL.plus(themeJson?.homepageBanner?.homepageBannerBgImg))
                                .into(_binding.ivBanner1)*/
                            val bannerList=ArrayList<SlideModel>()

                            for (i in 0 until themeJson?.homepageBanner?.homepageBannerBgImg?.size!!) {
                                val slideModel = SlideModel(ApiClient.ImageURL.BASE_URL.plus(themeJson.homepageBanner.homepageBannerBgImg[i]))
                                bannerList.add(slideModel)
                            }
                            _binding.bannerSlider.setImageList(bannerList, ScaleTypes.CENTER_CROP)


                            _binding.tvBannerTitle1.text =
                                themeJson?.homepageBanner?.homepageBannerTitleText
                            _binding.tvBannerHeader1.text =
                                themeJson?.homepageBanner?.homepageBannerHeading
                            _binding.tvBannerDescription1.text =
                                themeJson?.homepageBanner?.homepageBannerSubText
                            _binding.btnBanner1.text =
                                themeJson?.homepageBanner?.homepageBannerBtnText


                            _binding.tvNewsLatterTitle.text =
                                themeJson?.homepageNewsletter?.homepageNewsletterTitleText
                            _binding.tvNewsLetterSubText.text =
                                themeJson?.homepageNewsletter?.homepageNewsletterSubText
                            _binding.cbEmail.text =
                                themeJson?.homepageNewsletter?.homepageNewsletterDescription


                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun callBestseller() {
        Utils.showLoadingProgress(requireActivity())
        val bestseller = HashMap<String, String>()
        bestseller["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {



            Log.e("test", SharePreference.getStringPref(requireActivity(), SharePreference.token)!!)
            val response = if (SharePreference.getStringPref(requireActivity(), SharePreference.token).toString().isNullOrEmpty()) {
                    ApiClient.getClient(requireActivity())
                        .setBestSellerGuest(currentPageBestSeller.toString(), bestseller)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setBestSeller(currentPageBestSeller.toString(), bestseller)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bestsellerResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                if (Utils.isLogin(requireActivity())) {
                                    SharePreference.setStringPref(
                                        requireActivity(),
                                        SharePreference.cartCount,
                                        response.body.count.toString()
                                    )
                                    _binding.tvCount.text = response.body.count.toString()
                                }



                                _binding.rvBestsellers.show()

                                currentPageBestSeller = bestsellerResponse?.currentPage!!.toInt()
                                totalPagesBestSeller = bestsellerResponse.lastPage!!.toInt()
                                bestsellerResponse.data?.let {
                                    bestsellersList.addAll(it)
                                }


                                if (currentPageBestSeller >= totalPagesBestSeller) {
                                    isLastPageBestSeller = true
                                }
                                isLoadingBestSeller = false
                            } else {
                                _binding.rvBestsellers.hide()
                            }
                            bestSellersAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToken(requireActivity())
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }

    //Adapter set Best seller
    private fun bestsellerAdapter(bestsellersList: ArrayList<FeaturedProductsSub>) {
        _binding.rvBestsellers.layoutManager = managerBestsellers
        bestSellersAdapter =
            BestsellerAdapter(requireActivity(), bestsellersList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (bestsellersList[i].inWhishlist == false) {
                            callWishlist(
                                "add", bestsellersList[i].id, "BestsellersList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                bestsellersList[i].id,
                                "BestsellersList",
                                i
                            )
                        }
                    } else {
                        startActivity(Intent(requireActivity(), ActWelCome::class.java))
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = bestsellersList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, bestsellersList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = bestsellersList[i].id.toString()
                        addtocart["variant_id"] = bestsellersList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            bestsellersList[i].id,
                            bestsellersList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetail::class.java)
                    intent.putExtra(Constants.ProductId, bestsellersList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvBestsellers.adapter = bestSellersAdapter
    }


    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(requireActivity())
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setSafeOnClickListener {
            dialog.dismiss()
            startActivity(Intent(requireActivity(), ActShoppingCart::class.java))
        }
        confirmDialogBinding.tvContinueShopping.setSafeOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(requireActivity())) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        requireActivity(),
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = getString(R.string.theme_id)
                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    requireActivity(),
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                var cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                var count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i!!].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {

        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        requireActivity(),
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(), response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
                else -> {}
            }
        }
    }


    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(requireActivity())
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                when (itemType) {
                                    "CategoriesProduct" -> {
                                        //featuredProductsSubList[position].inWhishlist = true
                                        //  categoriesProductAdapter.notifyItemChanged(position)
                                    }
                                    "BestsellersList" -> {
                                        bestsellersList[position].inWhishlist = true
                                        bestSellersAdapter.notifyItemChanged(position)
                                    }
                                    "TrendingList" -> {
                                        trendingProductList[position].inWhishlist = true
                                        trendingProductAdapter.notifyItemChanged(position)
                                    }
                                }
                            } else {
                                when (itemType) {
                                    "CategoriesProduct" -> {
                                        // featuredProductsSubList[position].inWhishlist = false
                                        // categoriesProductAdapter.notifyItemChanged(position)
                                    }
                                    "BestsellersList" -> {
                                        bestsellersList[position].inWhishlist = false
                                        bestSellersAdapter.notifyItemChanged(position)
                                    }
                                    "TrendingList" -> {
                                        trendingProductList[position].inWhishlist = false
                                        trendingProductAdapter.notifyItemChanged(position)
                                    }
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }


    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {
        Log.e("Position", id.toString())
        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))

        val cartListData = SharePreference.getStringPref(
            requireActivity(),
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type

        var cartList =
            Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))

        if (cartList == null) {
              dlgConfirm(cartData.name+" "+R.string.addsuccess)
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)

            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            _binding.tvCount.text = cartDataList.size.toString()
        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            // Utils.errorAlert(requireActivity(), "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }
        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartList.size.toString()
            )
            _binding.tvCount.text = cartList.size.toString()
        }
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                            setCount(addtocart?.count)
                        }
                        0 -> {

                            if(response.body.data?.isOutOFStock==1)
                            {
                                Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                            }else {
                                dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)
                            }

                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                addtocart?.message.toString()
                            )

                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }


    override fun onResume() {
        super.onResume()
        lifecycleScope.launch {
            val currencyApi = async {
                callCurrencyApi()

            }

            currencyApi.await()
            callHeaderContentApi()

            currentPageBestSeller = 1
            bestsellersList.clear()
            bestsellerAdapter(bestsellersList)
            callBestseller()

            currentPageTrending = 1
            trendingProductList.clear()
            trendingProductAdapter(trendingProductList)
            callTrendingProducts()

        }



        _binding.tvCount.text =
            SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)

    }

}