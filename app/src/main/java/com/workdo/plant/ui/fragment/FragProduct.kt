package com.workdo.plant.ui.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.plant.R
import com.workdo.plant.adapter.CategoriesListAdapter
import com.workdo.plant.adapter.ProductCategoryAdapter
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.DlgConfirmBinding
import com.workdo.plant.databinding.FragProductBinding
import com.workdo.plant.model.FeaturedProductsSub
import com.workdo.plant.model.HomeCategoriesItem
import com.workdo.plant.model.ProductListItem
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.activity.ActMain
import com.workdo.plant.ui.activity.ActProductDetail
import com.workdo.plant.ui.activity.ActShoppingCart
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.PaginationScrollListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class FragProduct : Fragment() {
    private lateinit var _binding: FragProductBinding
    private var manager: GridLayoutManager? = null
    private var homeCategoriesList = ArrayList<HomeCategoriesItem>()
    private lateinit var allCategoriesAdapter: CategoriesListAdapter
    internal var isLoadingCategorirs = false
    internal var isLastPageCategorirs = false
    private var currentPageCategorirs = 1

    private var total_pagesCategorirs: Int = 0
    private var featuredProductsSubList = ArrayList<FeaturedProductsSub>()
    private var managerAllCategories: LinearLayoutManager? = null
    private lateinit var categoriesProductAdapter: ProductCategoryAdapter
    internal var isLoading = false
    private var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    var countItem = ""
    var count = 1
    var subID = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragProductBinding.inflate(layoutInflater)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }


    @SuppressLint("NotifyDataSetChanged")
    private fun init() {
        (requireActivity() as ActMain).bottomNavigationSelection("2")

        _binding.ivCart.setSafeOnClickListener {
            startActivity(Intent(requireActivity(), ActShoppingCart::class.java))
        }

        manager = GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        managerAllCategories =
            GridLayoutManager(requireActivity(), 2, GridLayoutManager.VERTICAL, false)

        paginationCategories()
        nestedScrollViewPagination()
        _binding.tvCount.text =
            SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
        _binding.clcart.setSafeOnClickListener {
            startActivity(Intent(requireActivity(), ActShoppingCart::class.java))

        }
        productBannerApi()

    }

    //TODO Product banner api
    private fun productBannerApi() {
        Utils.showLoadingProgress(requireActivity())
        val productBanner = HashMap<String, String>()
        productBanner["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .productBanner(productBanner)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bannersResponse = response.body.data?.themJson?.productsHeaderBanner
                    when (response.body.status) {
                        1 -> {
                            Glide.with(requireActivity())
                                .load(ApiClient.ImageURL.BASE_URL.plus(bannersResponse?.productsHeaderBanner))
                                .placeholder(R.drawable.banner_placeholder)
                                .into(_binding.ivProductBanner)
                            _binding.tvmess.text =
                                bannersResponse?.productsHeaderBannerTitleText.toString()
                        }
                        0 -> {

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO adapter set Categorirs
    private fun categoriesAdapter(homeCategoriesList: ArrayList<HomeCategoriesItem>) {
        _binding.rvAllCategories.layoutManager = manager
        allCategoriesAdapter =
            CategoriesListAdapter(requireActivity(), homeCategoriesList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    subID = homeCategoriesList[i].id.toString()
                    currentPage = 1
                    featuredProductsSubList.clear()
                    callCategorysProduct()
                }
            }
        _binding.rvAllCategories.adapter = allCategoriesAdapter
    }

    //TODO All Categories pagination
    private fun paginationCategories() {

        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPageCategorirs
            }

            override fun isLoading(): Boolean {
                return isLoadingCategorirs
            }

            override fun loadMoreItems() {
                isLoadingCategorirs = true
                currentPageCategorirs++
                callCategories()
            }
        }
        _binding.rvAllCategories.addOnScrollListener(paginationListener)
    }


    //TODO  Categories api callig
    private fun callCategories() {
        Utils.showLoadingProgress(requireActivity())
        val categorieshashmap = HashMap<String, String>()
        categorieshashmap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setHomeCategorys(currentPageCategorirs.toString(), categorieshashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvAllCategories.show()
                                currentPageCategorirs =
                                    categoriesResponse?.currentPage!!.toInt()
                                total_pagesCategorirs =
                                    categoriesResponse.lastPage!!.toInt()
                                if(currentPageCategorirs==1)
                                {
                                    homeCategoriesList.add(HomeCategoriesItem(name = "All Product", id = 0))

                                }
                                categoriesResponse.data?.let {
                                    homeCategoriesList.addAll(it)
                                }
                                subID = homeCategoriesList[0].id.toString()
                                currentPage = 1
                                featuredProductsSubList.clear()
                                callCategorysProduct()
                                if (currentPageCategorirs >= total_pagesCategorirs) {
                                    isLastPageCategorirs = true
                                }
                                isLoadingCategorirs = false
                            } else {
                                _binding.rvAllCategories.hide()
                            }
                            allCategoriesAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun nestedScrollViewPagination() {
        _binding.rvFeaturedProduct.isNestedScrollingEnabled = false
        _binding.scrollView.viewTreeObserver?.addOnScrollChangedListener {
            val view = _binding.scrollView.getChildAt(_binding.scrollView.childCount - 1)
            val diff = view.bottom - (_binding.scrollView.height + _binding.scrollView.scrollY)
            if (diff == 0 && !isLoading && !isLastPage) {
                isLoading = true
                currentPage++
                callCategorysProduct()
            }
        }
    }


    //TODO category product api calling
    private fun callCategorysProduct() {
        Utils.showLoadingProgress(requireActivity())
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["subcategory_id"] = "0"
        categoriesProduct["maincategory_id"] = subID
        categoriesProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        .isNullOrEmpty()
                ) {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProductGuest(currentPage.toString(), categoriesProduct)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setCategorysProduct(currentPage.toString(), categoriesProduct)
                }
            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                countItem = categoriesProductResponse?.data?.size.toString()
                                _binding.rvFeaturedProduct.show()
                                _binding.vieww.hide()
                                currentPage =
                                    categoriesProductResponse?.currentPage!!.toInt()
                                total_pages =
                                    categoriesProductResponse.lastPage!!.toInt()
                                categoriesProductResponse.data?.let {
                                    featuredProductsSubList.addAll(it)
                                }
                                Log.d("Current Page", currentPage.toString())
                                Log.d("total_pages", total_pages.toString())
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvFeaturedProduct.hide()
                                _binding.vieww.show()
                            }
                            categoriesProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesProductResponse?.message.toString()
                            )

                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                        }
                    }
                    if (categoriesProductResponse?.to == null) {

                        _binding.tvFoundproduct.text =
                            getString(R.string.found).plus(" ").plus("0").plus(" ")
                                .plus(getString(R.string.product))
                    } else {
                        _binding.tvFoundproduct.text = getString(R.string.found).plus(" ").plus(
                            categoriesProductResponse.total
                        ).plus(" ").plus(getString(R.string.product))
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO Adapter set Categories Product
    private fun categoriesProductsAdapter(featuredProductsSubList: ArrayList<FeaturedProductsSub>) {

        _binding.rvFeaturedProduct.layoutManager = managerAllCategories
        countItem = featuredProductsSubList.size.toString()
        categoriesProductAdapter =
            ProductCategoryAdapter(
                requireActivity(),
                featuredProductsSubList
            ) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (featuredProductsSubList[i].inWhishlist == false) {
                            callWishlist(
                                "add", featuredProductsSubList[i].id, "CategoriesProduct", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                featuredProductsSubList[i].id,
                                "CategoriesProduct",
                                i
                            )
                        }
                    } else {
                        startActivity(Intent(requireActivity(), ActWelCome::class.java))
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(
                            featuredProductsSubList[i],
                            featuredProductsSubList[i].id
                        )
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(
                                requireActivity(),
                                SharePreference.userId
                            )
                                .toString()
                        addtocart["product_id"] = featuredProductsSubList[i].id.toString()
                        addtocart["variant_id"] =
                            featuredProductsSubList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)

                        addtocartApi(
                            addtocart,
                            featuredProductsSubList[i].id,
                            featuredProductsSubList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetail::class.java)
                    intent.putExtra(Constants.ProductId, featuredProductsSubList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvFeaturedProduct.adapter = categoriesProductAdapter

    }

    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {

        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))
        val cartListData = SharePreference.getStringPref(
            requireActivity(),
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type
        var cartList = Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))
        if (cartList == null) {
            dlgConfirm(cartData.name.plus(" ").plus("add successfully."))

            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            setCount(cartDataList.size)
        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            //Utils.errorAlert(requireActivity(), "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }

        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartList.size.toString()
            )

            setCount(cartList.size)
        }
    }

    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    //TODO add to cart api
    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            setCount(addtocart?.count)
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                        }
                        0 -> {
                            if(response.body.data?.isOutOFStock==1)
                            {
                                Utils.errorAlert(requireActivity(), response.body.data.message.toString())
                            }else {
                                dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)
                            }
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                addtocart?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))

                            activity?.finish()
                            activity?.finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(requireActivity())
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setSafeOnClickListener {
            dialog.dismiss()
            startActivity(Intent(requireActivity(), ActShoppingCart::class.java))

        }
        confirmDialogBinding.tvContinueShopping.setSafeOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(requireActivity())) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        requireActivity(),
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = getString(R.string.theme_id)
                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    requireActivity(),
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                val cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                val count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        requireActivity(),
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {

                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(), response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {
        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    //TODO wishlist api
    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(requireActivity())
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                .toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                if (itemType == "CategoriesProduct") {
                                    featuredProductsSubList[position].inWhishlist = true
                                    categoriesProductAdapter.notifyItemChanged(position)
                                }
                            } else {
                                if (itemType == "CategoriesProduct") {
                                    featuredProductsSubList[position].inWhishlist = false
                                    categoriesProductAdapter.notifyItemChanged(position)
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }


    override fun onResume() {
        super.onResume()
        currentPageCategorirs = 1
        currentPage=1
        homeCategoriesList.clear()
        featuredProductsSubList.clear()

        categoriesAdapter(homeCategoriesList)
        callCategories()
        _binding.tvCount.text = SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
        categoriesProductsAdapter(featuredProductsSubList)
    }


}