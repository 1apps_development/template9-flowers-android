package com.workdo.plant.ui.fragment

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import com.workdo.plant.R
import com.workdo.plant.api.ApiClient
import com.workdo.plant.ui.activity.*
import com.workdo.plant.databinding.FragSettingsBinding
import com.workdo.plant.model.EditProfileUpdateModel
import com.workdo.plant.model.SingleResponse
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.authentication.ActChangePassword
import com.workdo.plant.ui.authentication.ActWelCome
import com.workdo.plant.util.Common.setImageUpload
import com.workdo.plant.util.Common.setRequestBody
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import kotlinx.coroutines.launch
import java.io.File


class FragSetting : Fragment() {
    private lateinit var _binding: FragSettingsBinding
    var userId: String = ""
    private var imageFile: File? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding= FragSettingsBinding.inflate(layoutInflater)
        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onResume() {
        super.onResume()

        if(Utils.isLogin(requireActivity()))
        {
            _binding.tvUserEmail.text = SharePreference.getStringPref(requireActivity(), SharePreference.userEmail)
            _binding.tvUserName.text =  SharePreference.getStringPref(requireActivity(), SharePreference.userFirstName).plus(" ").plus(SharePreference.getStringPref(requireActivity(), SharePreference.userLastName))
        }else
        {
            _binding.tvUserName.text=resources.getString(R.string.guest)
            _binding.tvUserEmail.text=resources.getString(R.string.guest_email)
        }
        _binding.tvCount.text = SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
        if (imageFile == null) {
            val baseUrl=SharePreference.getStringPref(requireActivity(),SharePreference.PaymentUrl)

            Glide.with(requireActivity())
                .load(
                    baseUrl.plus(
                    SharePreference.getStringPref(requireActivity(), SharePreference.userProfile))).placeholder(R.drawable.placeholder)
                .into(_binding.ivUserImage)
        }
    }


    private fun init() {
        Log.e(
            "Count",
            SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
        )

        userId = SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
        if (userId != "") {
            _binding.clMyLogout.show()
            _binding.clMyLogin.hide()

        } else {
            _binding.clMyLogout.hide()
            _binding.clMyLogin.show()
            _binding.tvUserName.text="Guest"
            _binding.tvUserEmail.text="guest@gmail.com"
        }

        _binding.clHistory.setSafeOnClickListener {
            if(Utils.isLogin(requireActivity()))
            {
                startActivity(Intent(requireActivity(),ActOrderHistory::class.java))
            }else
            {
                Utils.openWelcomeScreen(requireActivity())
            }
        }
        _binding.btnEditProfile.setSafeOnClickListener {
            if(Utils.isLogin(requireActivity()))
            {
                startActivity(Intent(requireActivity(),ActPersonalDetail::class.java))

            }else
            {
                Utils.openWelcomeScreen(requireActivity())


            }
        }
        _binding.clEditProfile.setSafeOnClickListener {
            if(Utils.isLogin(requireActivity()))
            {
                startActivity(Intent(requireActivity(),ActPersonalDetail::class.java))

            }else
            {
                Utils.openWelcomeScreen(requireActivity())

            }
        }
        _binding.clPassword.setSafeOnClickListener {
            if(Utils.isLogin(requireActivity()))
            {
                startActivity(Intent(requireActivity(),ActChangePassword::class.java))


            }else
            {
                Utils.openWelcomeScreen(requireActivity())

            }
        }
        _binding.clAddress.setSafeOnClickListener {

            if(Utils.isLogin(requireActivity()))
            {
                startActivity(Intent(requireActivity(),ActGetAddress::class.java))
            }else
            {
                Utils.openWelcomeScreen(requireActivity())

            }
        }
        _binding.clcart.setSafeOnClickListener {
            startActivity(Intent(requireActivity(),ActShoppingCart::class.java))

        }
        _binding.ivMenu.setSafeOnClickListener {
            startActivity(Intent(requireActivity(),ActMenu::class.java))

        }
        _binding.clMyLogout.setSafeOnClickListener {
            mLogoutDialog()
        }
        _binding.clMyLogin.setSafeOnClickListener {
            Utils.openWelcomeScreen(requireActivity())

        }
        _binding.ivUserImage.setSafeOnClickListener {

            if(Utils.isLogin(requireActivity())) {
                ImagePicker.with(this)
                    .cropSquare()
                    .compress(1024)
                    .saveDir(
                        File(requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), resources.getString(R.string.app_name))
                    )
                    .maxResultSize(1080, 1080)
                    .createIntent { intent ->
                        startForProfileImageResult.launch(intent)
                    }
            }else
            {
                Utils.openWelcomeScreen(requireActivity())

            }
        }
        _binding.clMyReturn.setSafeOnClickListener {
            startActivity(Intent(requireActivity(),ActMyReturns::class.java))

        }
        _binding.clLoyalty.setSafeOnClickListener {

            startActivity(Intent(requireActivity(),ActLoyalty::class.java))
        }
    }







    // Todo OnActivityResult Profile  Picture Update
    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data
            when (resultCode) {
                Activity.RESULT_OK -> {
                    //Image Uri will not be null for RESULT_OK
                    val fileUri = data?.data!!
                    Log.e("FilePath", fileUri.path.toString())
                    fileUri.path.let { imageFile = File(it) }
                    Log.e("imageFileLength", imageFile!!.length().toString())
                    Glide.with(requireActivity()).load(fileUri.path)
                        .into(_binding.ivUserImage)
                    callSetProfile()
                }
                ImagePicker.RESULT_ERROR -> {
                    Toast.makeText(
                        requireActivity(),
                        ImagePicker.getError(data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }


    //TODO set profile data
    private fun callSetProfile() {
        Utils.showLoadingProgress(requireActivity())
        val profile =getString(R.string.theme_id)
        var responseData: NetworkResponse<EditProfileUpdateModel, SingleResponse>? = null
        lifecycleScope.launch {
            if (imageFile != null) {
                responseData = ApiClient.getClient(requireActivity()).setUpdateUserImage(
                    setRequestBody(profile),
                    setRequestBody(
                        SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                            .toString()
                    ),
                    setImageUpload("image", imageFile!!)
                )
            }

            when (responseData) {
                is NetworkResponse.Success -> {

                    val response =
                        (responseData as NetworkResponse.Success<EditProfileUpdateModel>).body.data
                    Utils.dismissLoadingProgress()

                    SharePreference.setStringPref(
                        requireActivity(), SharePreference.userProfile,
                        response?.message.toString().toString()
                    )
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if ((responseData as NetworkResponse.ApiError<SingleResponse>).body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    }else{
                        Utils.errorAlert(
                            requireActivity(),
                            (responseData as NetworkResponse.ApiError<SingleResponse>).body.message.toString()
                        )
                    }
                }
                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }
                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.something_went_wrong)
                    )
                }

                else -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.something_went_wrong)
                    )
                }
            }
        }
    }

    private fun callLogout(logout: HashMap<String, String>) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            val response = ApiClient.getClient(requireActivity())
                .setLogout(logout)
            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val logout = response.body
                    when (response.body.status) {
                        1 -> {
                            val youtube=SharePreference.getStringPref(requireActivity(), SharePreference.youtube) ?: ""
                            val insta=SharePreference.getStringPref(requireActivity(), SharePreference.insta) ?: ""
                            val messanger=SharePreference.getStringPref(requireActivity(), SharePreference.messanger) ?: ""
                            val twitter=SharePreference.getStringPref(requireActivity(), SharePreference.twitter) ?: ""
                            val returnPolicy=SharePreference.getStringPref(requireActivity(), SharePreference.returnPolicy) ?: ""
                            val Contact_Us=SharePreference.getStringPref(requireActivity(), SharePreference.Contact_Us) ?: ""
                            val Terms=SharePreference.getStringPref(requireActivity(), SharePreference.Terms) ?: ""
                            val baseUrl=SharePreference.getStringPref(requireActivity(), SharePreference.BaseUrl) ?: ""
                            val imageUrl=SharePreference.getStringPref(requireActivity(), SharePreference.ImageUrl) ?: ""
                            val paymentUrl=SharePreference.getStringPref(requireActivity(), SharePreference.PaymentUrl) ?: ""


                            val preference = SharePreference(requireActivity())
                            preference.mLogout()
                            SharePreference.setStringPref(requireActivity(), SharePreference.youtube, youtube)
                            SharePreference.setStringPref(requireActivity(), SharePreference.insta, insta)
                            SharePreference.setStringPref(requireActivity(), SharePreference.messanger, messanger)
                            SharePreference.setStringPref(requireActivity(), SharePreference.twitter, twitter)
                            SharePreference.setStringPref(requireActivity(), SharePreference.returnPolicy, returnPolicy)
                            SharePreference.setStringPref(requireActivity(), SharePreference.Contact_Us, Contact_Us)
                            SharePreference.setStringPref(requireActivity(), SharePreference.Terms, Terms)
                            SharePreference.setStringPref(requireActivity(), SharePreference.BaseUrl, baseUrl)
                            SharePreference.setStringPref(requireActivity(), SharePreference.ImageUrl, imageUrl)
                            SharePreference.setStringPref(requireActivity(), SharePreference.PaymentUrl, paymentUrl)
                            val intent = Intent(activity, ActWelCome::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            activity?.startActivity(intent)
                            activity?.finish()
                        }

                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                logout.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                logout.message.toString()
                            )
                            startActivity(Intent(requireActivity(),ActWelCome::class.java))
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    }else{
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun mLogoutDialog() {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.logoutdialog)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val logout = HashMap<String, String>()
            logout["user_id"] =
                SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
            logout["theme_id"]=getString(R.string.theme_id)
            callLogout(logout)
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
}