package com.workdo.plant.ui.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.plant.R
import com.workdo.plant.adapter.WishlistsAdapter
import com.workdo.plant.api.ApiClient
import com.workdo.plant.databinding.DlgConfirmBinding
import com.workdo.plant.databinding.FragWishListBinding
import com.workdo.plant.model.WishListDataItem
import com.workdo.plant.remote.NetworkResponse
import com.workdo.plant.ui.activity.ActProductDetail
import com.workdo.plant.ui.activity.ActShoppingCart
import com.workdo.plant.util.Constants
import com.workdo.plant.util.ExtensionFunctions.hide
import com.workdo.plant.util.ExtensionFunctions.setSafeOnClickListener
import com.workdo.plant.util.ExtensionFunctions.show
import com.workdo.plant.util.PaginationScrollListener
import com.workdo.plant.util.SharePreference
import com.workdo.plant.util.Utils
import kotlinx.coroutines.launch

class FragWishList : Fragment() {
   private lateinit var _binding:FragWishListBinding
    internal var isLoading = false
    internal var isLastPage = false
    private var currentPage = 1
    private var total_pages: Int = 0
    private var wishList = ArrayList<WishListDataItem>()
    private lateinit var wishlistAdapter: WishlistsAdapter

    private var manager: LinearLayoutManager? = null
    var count = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding= FragWishListBinding.inflate(layoutInflater)
        return _binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }


    @SuppressLint("NotifyDataSetChanged")
    private fun init() {
        manager = LinearLayoutManager(requireActivity())

        pagination()
        _binding.tvCount.text =
            SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
        _binding.clcart.setSafeOnClickListener {
            startActivity(Intent(requireActivity(),ActShoppingCart::class.java))
        }

    }

    private fun pagination() {
        val paginationListener = object : PaginationScrollListener(manager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                callWishList()
            }
        }
        _binding.rvWishlist.addOnScrollListener(paginationListener)
    }

    //TODO wishlist api
    private fun callWishList() {
        Utils.showLoadingProgress(requireActivity())
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["user_id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
        categoriesProduct["theme_id"]=getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .getWishlist(currentPage.toString(), categoriesProduct)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addressListResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvWishlist.show()
                                _binding.tvNoDataFound.hide()
                               currentPage =
                                    addressListResponse?.currentPage!!.toInt()
                                total_pages =
                                    addressListResponse.lastPage!!.toInt()
                                addressListResponse.data?.let {
                                    wishList.addAll(it)
                                }
                                if (currentPage >= total_pages) {
                                    isLastPage = true
                                }
                                isLoading = false
                            } else {
                                _binding.rvWishlist.hide()
                                _binding.tvNoDataFound.show()
                            }
                            wishlistAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                addressListResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                response.body.message.toString()
                            )
                            Utils.openWelcomeScreen(requireActivity())

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        currentPage = 1
        isLastPage = false
        isLoading = false
        wishList.clear()
        wishListAdapter(wishList)
        callWishList()
        _binding.tvCount.text =
            SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
    }

    //TODO wishlist list set
    private fun wishListAdapter(wishList: ArrayList<WishListDataItem>) {
        _binding.rvWishlist.layoutManager = manager
        wishlistAdapter =
            WishlistsAdapter(requireActivity(), wishList) { i: Int, s: String ->
                if (s == Constants.ItemDelete) {
                    val wishlistRemove = HashMap<String, String>()
                    wishlistRemove["user_id"] =
                        SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                            .toString()
                    wishlistRemove["product_id"] = wishList[i].productId.toString()
                    wishlistRemove["wishlist_type"] = "remove"
                    wishlistRemove["theme_id"]=getString(R.string.theme_id)
                    callremoveWishListApi(wishlistRemove, i)
                } else if (s == Constants.CartClick) {
                    val addtocart = HashMap<String, String>()
                    addtocart["user_id"] =
                        SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                            .toString()
                    addtocart["product_id"] = wishList[i].productId.toString()
                    addtocart["variant_id"] = wishList[i].variantId.toString()
                    addtocart["qty"] = "1"
                    addtocart["theme_id"]=getString(R.string.theme_id)
                    addtocartApi(addtocart, i,wishList[i].variantId)
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetail::class.java)
                    intent.putExtra(Constants.ProductId, wishList[i].productId.toString())
                    startActivity(intent)
                }
            }
        _binding.rvWishlist.adapter = wishlistAdapter
    }

    //TODO add tocart api
    private fun addtocartApi(addtocart: HashMap<String, String>, i: Int, variantId: Int?) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartData = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.cartCount,
                                cartData?.count.toString()
                            )
                            setCount(cartData?.count)
                            wishlistAdapter.notifyItemChanged(i)
                        }
                        0 -> {
                            if(response.body.data?.isOutOFStock==1)
                            {
                                Utils.errorAlert(requireActivity(), response.body.data?.message.toString())
                            }else
                            {
                                dlgAlreadyCart(response.body.data?.message, wishList[i].productId, variantId)

                            }

                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartData?.message.toString()
                            )
                            Utils.openWelcomeScreen(requireActivity())

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO checkput or continue shopping dialog
    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(requireActivity())
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setSafeOnClickListener {
            dialog.dismiss()
            startActivity(Intent(requireActivity(),ActShoppingCart::class.java))
        }
        confirmDialogBinding.tvContinueShopping.setSafeOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    //TODO item already cart dialog
    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()
            count = 1
            cartqty["product_id"] = id.toString()
            cartqty["user_id"] =
                SharePreference.getStringPref(
                    requireActivity(),
                    SharePreference.userId
                ).toString()
            cartqty["variant_id"] = defaultVariantId.toString()
            cartqty["quantity_type"] = "increase"
            cartqty["theme_id"]=getString(R.string.theme_id)
            cartQtyApi(cartqty, "increase")

        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    //TODO qty api
    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        requireActivity(),
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            Utils.dismissLoadingProgress()
                            wishlistAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                            Utils.openWelcomeScreen(requireActivity())
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(), response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }

    //TODO Wishlist itemremove api
    private fun callremoveWishListApi(wishlistRemove: HashMap<String, String>, position: Int) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setWishlist(wishlistRemove)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            currentPage = 1
                            isLastPage = false
                            isLoading = false
                            wishList.clear()
                            callWishList()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                            Utils.openWelcomeScreen(requireActivity())

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToken(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

}