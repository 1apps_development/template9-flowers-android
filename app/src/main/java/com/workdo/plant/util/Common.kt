package com.workdo.plant.util

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.Telephony
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.workdo.plant.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

object Common {
    var isProfileEdit: Boolean = false
    var isProfileMainEdit: Boolean = false
    var isCartTrue: Boolean = false
    var isCartTrueOut: Boolean = false
    var isCancelledOrder: Boolean = false
    var isAddOrUpdated: Boolean = false
    fun getToast(activity: Activity, strTxtToast: String) {
        Toast.makeText(activity, strTxtToast, Toast.LENGTH_SHORT).show()
    }

    fun getLog(strKey: String, strValue: String) {
        Log.e(">>>---  $strKey  ---<<<", strValue)
    }

    fun isValidEmail(strPattern: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(strPattern).matches();
    }

    fun isValidAmount(strPattern: String): Boolean {
        return Pattern.compile(
            "^[0-9]+([.][0-9]{2})?\$"
        ).matcher(strPattern).matches();
    }

    fun isValidNumber(strPattern: String): Boolean {
        return Pattern.compile(
            "0123456789."
        ).matcher(strPattern).matches()
    }

    @SuppressLint("MissingPermission")
    fun isCheckNetwork(context: Context): Boolean {
        val connectivityManager = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }



    var dialog: Dialog? = null

    fun dismissLoadingProgress() {
        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
            dialog = null
        }
    }

    fun showLoadingProgress(context: Activity) {
        if (dialog != null) {
            dialog!!.dismiss()
            dialog = null
        }
        dialog = Dialog(context)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.setContentView(R.layout.dlg_progress)
        dialog!!.setCancelable(false)
        dialog!!.show()
    }
    /* Todo Replace Fragment*/

    fun replaceFragment(manager: FragmentManager, fragment: Fragment, frameId: Int = 0) {
        val transaction = manager.beginTransaction()
        transaction.replace(frameId, fragment, fragment.javaClass.simpleName)
        transaction.commit()

    }

     fun showFullScreenBottomSheet(bottomSheet: FrameLayout) {
        val layoutParams = bottomSheet.layoutParams
        layoutParams.height = Resources.getSystem().displayMetrics.heightPixels
        bottomSheet.layoutParams = layoutParams
    }

     fun expandBottomSheet(bottomSheetBehavior: BottomSheetBehavior<FrameLayout>) {
        bottomSheetBehavior.skipCollapsed = true
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }


/*    fun getCurrentLanguage(context: Activity, isChangeLanguage: Boolean) {
        if (getStringPref(context, SELECTED_LANGUAGE) == null || getStringPref(
                context,
                SELECTED_LANGUAGE
            ).equals("", true)
        ) {
            setStringPref(
                context,
                SELECTED_LANGUAGE,
                context.resources.getString(R.string.language_english)
            )
        }
        val locale = if (getStringPref(
                context,
                SELECTED_LANGUAGE
            ).equals(context.resources.getString(R.string.language_english), true)
        ) {
            Locale("en-us")
        } else {
            Locale("ar")
        }
        val activityRes = context.resources
        val activityConf = activityRes.configuration
        if (getStringPref(context, SELECTED_LANGUAGE).equals(context.resources.getString(R.string.language_english), true)) {
            activityConf.setLocale(Locale("en-us")) // API 17+ only.
        } else {
            activityConf.setLocale(Locale("ar"))
        }
        activityRes.updateConfiguration(activityConf, activityRes.displayMetrics)
        val applicationRes = context.applicationContext.resources
        val applicationConf = applicationRes.configuration
        applicationConf.setLocale(locale)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.createConfigurationContext(applicationConf)
        } else {
            context.resources.updateConfiguration(
                applicationConf,
                context.resources.displayMetrics
            )
        }
        if (isChangeLanguage) {
            val intent = Intent(context, DashBoard::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
            context.finish()
            context.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
        }
    }*/

    fun successAlert(activity: Activity, msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }


    fun errorAlert(activity: Activity, msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("NewApi", "SimpleDateFormat")
    fun getDayAndMonth(strDate: String): String {
        val sd = SimpleDateFormat("dd-MM-yyyy")
        val sdout = SimpleDateFormat("dd-MMMM-yyyy")
        val sdday = SimpleDateFormat("EEEE")
        val date: Date = sd.parse(strDate)!!
        val getDay = sdday.format(date)
        val getDate = sdout.format(date)
        val stringArray = getDate.split("-").toTypedArray()
        val strDay = stringArray.get(0).plus("th")
        return getDay.plus(" ".plus(stringArray.get(1))).plus(" ".plus(strDay))
    }

    fun setImageUpload(strParameter: String, mSelectedFileImg: File): MultipartBody.Part {
        return MultipartBody.Part.createFormData(
            strParameter,
            mSelectedFileImg.getName(),
            RequestBody.create("image/*".toMediaType(), mSelectedFileImg)
        )
    }

    fun setRequestBody(bodyData: String): RequestBody {
        return bodyData.toRequestBody("text/plain".toMediaType())
    }

/*
    fun setLogout(activity: Activity) {
        val getLanguage = getStringPref(activity, SELECTED_LANGUAGE)
        val isTutorialsActivity: Boolean =
            SharePreference.getBooleanPref(activity, SharePreference.isTutorial)
        val loginUserType =
            SharePreference.getStringPref(activity, SharePreference.UserLoginType) ?: ""
        val preference = SharePreference(activity)
        preference.mLogout()
        setBooleanPref(activity, SharePreference.isTutorial, isTutorialsActivity)
        setStringPref(activity, SharePreference.SELECTED_LANGUAGE, getLanguage!!)
        setStringPref(activity, SharePreference.UserLoginType, loginUserType!!)
        val intent = Intent(activity, ActLogin::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        activity.startActivity(intent)
        activity.finish()
    }
*/


    fun getDate(strDate: String): String {
        val curFormater = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        val dateObj = curFormater.parse(strDate)
        val postFormater = SimpleDateFormat("dd MMM yyyy", Locale.US)
        return postFormater.format(dateObj)
    }


    fun backEndDateFormat(strDate: String): String {
        val curFormater = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val dateObj = curFormater.parse(strDate)
        val postFormater = SimpleDateFormat("dd MMM yyyy", Locale.US)
        return postFormater.format(dateObj)
    }


    fun getDateFormat(strDate: String): String {
        val curFormater = SimpleDateFormat("dd MMM yyyy", Locale.US)
        val dateObj = curFormater.parse(strDate)
        val postFormater = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        return postFormater.format(dateObj)
    }

    fun timeFormat(time: String): String? {
        val timeFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        val date = timeFormat.parse(time)

        val userTimeFormat = SimpleDateFormat("hh:mm a", Locale.getDefault())
        return userTimeFormat.format(date)
    }

    fun getTimeFormat(time: String): String? {
        val userTimeFormat = SimpleDateFormat("hh:mm a", Locale.getDefault())

        val date = userTimeFormat.parse(time)
        val timeFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())

        return timeFormat.format(date)
    }

    fun closeKeyBoard(activity: Activity) {
        val view: View? = activity.currentFocus
        if (view != null) {
            try {
                val imm: InputMethodManager =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    fun openEmail(context: Context, email: String) {
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:")
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        intent.putExtra(Intent.EXTRA_SUBJECT, " ")
        intent.putExtra(Intent.EXTRA_TEXT, "")
        if (intent.resolveActivity(context.packageManager) != null) {
            context.startActivity(intent)
        }
    }

    fun openTextMessenger(context: Context, phoneNumber: String) {
        val defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(context)
        val sendIntent = Intent(Intent.ACTION_SEND)
        sendIntent.data = Uri.parse("smsto:${phoneNumber}")
        sendIntent.type = "text/plain"
        sendIntent.putExtra("address", phoneNumber)
        if (defaultSmsPackageName != null) {
            sendIntent.setPackage(defaultSmsPackageName)
        }
        context.startActivity(sendIntent)
    }

    fun openDialPad(context: Context, phoneNumber: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:${phoneNumber}")
        context.startActivity(intent)
    }


    fun getPrice(currencyPos: String, currency: String, price: String): String {
        return if (currencyPos == "left") {
            currency.plus(String.format(Locale.US, "%,.02f", price.toDouble()))
        } else {
            String.format(Locale.US, "%,.02f", price.toDouble()).plus(currency)
        }
    }

    fun getDiscountType(discountType: String): String {
        return if (discountType == "2") {
            "%"
        } else {
            ""
        }
    }


    @SuppressLint("PackageManagerGetSignatures")
    fun printKeyHash(context: Activity): String? {
        val packageInfo: PackageInfo
        var key: String? = null
        try {
            //getting application package name, as defined in manifest
            val packageName = context.applicationContext.packageName

            //Retriving package info
            packageInfo = context.packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            Log.e("Package Name=", context.applicationContext.packageName)
            for (signature in packageInfo.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                key = String(Base64.encode(md.digest(), 0))

                // String key = new String(Base64.encodeBytes(md.digest()));
                //getLog("Key Hash", key)
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("Name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("No such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("Exception", e.toString())
        }
        return key
    }

}