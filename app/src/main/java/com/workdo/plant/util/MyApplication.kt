package com.workdo.plant.util

import android.app.Application
import androidx.multidex.MultiDex

class MyApplication : Application() {


    companion object {
        lateinit var app: MyApplication



        fun getInstance(): MyApplication {
            return app
        }
    }
    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        app=this

    }


}