package com.workdo.plant.util;

import com.workdo.plant.model.CityListData;

public interface OnItemClickListenerCity {
    void onItemClickCity(CityListData item);
}
