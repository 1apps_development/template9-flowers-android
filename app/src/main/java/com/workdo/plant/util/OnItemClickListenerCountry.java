package com.workdo.plant.util;

import com.workdo.plant.model.CountryDataItem;

public interface OnItemClickListenerCountry {
    void onItemClick(CountryDataItem item);
}
