package com.workdo.plant.util;

import com.workdo.plant.model.CityListData;

public interface OnItemClickListenerGuestCity {
    void onItemClickCityGuest(CityListData item);
}
