package com.workdo.plant.util;

import com.workdo.plant.model.CountryDataItem;

public interface OnItemClickListenerGuestCountry {
    void onItemClickGuest(CountryDataItem item);
}
