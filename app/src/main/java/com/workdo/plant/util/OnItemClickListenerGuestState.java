package com.workdo.plant.util;

import com.workdo.plant.model.StateListData;

public interface OnItemClickListenerGuestState {
    void onItemClickStateGuest(StateListData item);
}
