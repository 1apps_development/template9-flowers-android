package com.workdo.plant.util;

import com.workdo.plant.model.StateListData;

public interface OnItemClickListenerState {
    void onItemClickState(StateListData item);
}
