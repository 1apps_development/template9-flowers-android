package com.workdo.plant.util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.util.Log
import android.view.Window
import android.widget.Toast
import com.workdo.plant.util.SharePreference.Companion.setStringPref
import com.workdo.plant.R
import com.workdo.plant.ui.authentication.ActWelCome
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


object Utils {
    var dialog: Dialog? = null
    fun dismissLoadingProgress() {
        if (dialog != null && dialog?.isShowing == true) {
            dialog?.dismiss()
        }
    }


    fun successAlert(activity: Activity, message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    fun errorAlert(activity: Activity, message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

    }

    fun getLog(strKey: String, strValue: String) {
        Log.e(">>>---  $strKey  ---<<<", strValue)
    }

    fun isCheckNetwork(context: Context): Boolean {
        val connectivityManager = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun isLogin(context: Context): Boolean {
        return !SharePreference.getStringPref(context, SharePreference.userId).isNullOrEmpty()
    }

    fun isValidEmail(strPattern: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(strPattern).matches()
    }


    fun showLoadingProgress(context: Activity?) {
        if (dialog != null) {
            dialog?.dismiss()
            dialog = null
        }
        dialog = context?.let { Dialog(it) }
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.setContentView(R.layout.dlg_progress)
        dialog?.setCancelable(false)
        dialog?.show()
    }


    fun getPrice(price: String): String {
        return String.format(Locale.US, "%,.02f", price.toDouble())

    }

    fun getStringPreference(context: Activity?, string: String): String? {
        return context?.let { SharePreference.getStringPref(it, string) }
    }

    fun setImageUpload(strParameter: String, mSelectedFileImg: File): MultipartBody.Part {
        return MultipartBody.Part.createFormData(
            strParameter,
            mSelectedFileImg.getName(),
            mSelectedFileImg.asRequestBody("image/*".toMediaType())
        )
    }

    fun setRequestBody(bodyData: String): RequestBody {
        return bodyData.toRequestBody("text/plain".toMediaType())
    }

    fun getDate(strDate: String): String {
        val curFormater = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val dateObj = curFormater.parse(strDate)
        val postFormater = SimpleDateFormat("MMM dd, yyyy", Locale.US)
        return postFormater.format(dateObj)
    }

    fun setInvalidToken(activity: Activity) {
        val getUserID: String? = SharePreference.getStringPref(activity, SharePreference.userId)
        val preference = SharePreference(activity)
        preference.mLogout()
        if (getUserID != null) {
            setStringPref(activity, SharePreference.userId, "")
        }
        val intent = Intent(activity, ActWelCome::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        activity.startActivity(intent)
        activity.finish()
    }

    fun logOut(context: Context) {
        val youtube = SharePreference.getStringPref(context, SharePreference.youtube) ?: ""
        val insta = SharePreference.getStringPref(context, SharePreference.insta) ?: ""
        val messanger = SharePreference.getStringPref(context, SharePreference.messanger) ?: ""
        val twitter = SharePreference.getStringPref(context, SharePreference.twitter) ?: ""
        val returnPolicy =
            SharePreference.getStringPref(context, SharePreference.returnPolicy) ?: ""
        val Contact_Us = SharePreference.getStringPref(context, SharePreference.Contact_Us) ?: ""
        val Terms = SharePreference.getStringPref(context, SharePreference.Terms) ?: ""
        val baseUrl = SharePreference.getStringPref(context, SharePreference.BaseUrl) ?: ""
        val imageUrl = SharePreference.getStringPref(context, SharePreference.ImageUrl) ?: ""
        val paymentUrl = SharePreference.getStringPref(context, SharePreference.PaymentUrl) ?: ""


        val preference = SharePreference(context)
        preference.mLogout()
        setStringPref(context, SharePreference.youtube, youtube)
        setStringPref(context, SharePreference.insta, insta)
        setStringPref(context, SharePreference.messanger, messanger)
        setStringPref(context, SharePreference.twitter, twitter)
        setStringPref(context, SharePreference.returnPolicy, returnPolicy)
        setStringPref(context, SharePreference.Contact_Us, Contact_Us)
        setStringPref(context, SharePreference.Terms, Terms)
        setStringPref(context, SharePreference.BaseUrl, baseUrl)
        setStringPref(context, SharePreference.ImageUrl, imageUrl)
        setStringPref(context, SharePreference.PaymentUrl, paymentUrl)

        val intent = Intent(context, ActWelCome::class.java)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(intent)

    }

    fun openWelcomeScreen(context: Activity) {
        val intent = Intent(context, ActWelCome::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(intent)
        context.finish()
    }

}